<?php

if(isset($_SESSION['user']))
{
 echo '<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <h2 class="sub-header"> <center> Sugestôes </center> </h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Título</th>
                  <th>Tipo</th>
                  <th>Prévia</th>
                  <th>Aprovar</th>
                  <th>Negar</th>
                </tr>
              </thead>
              <tbody>';
              foreach($sugestoes as $exibir)
              {
                echo '<tr>
                  <td>'.wordwrap($exibir['titulo'],16,'<br>',1).'</td>
                  <td>'.$exibir['tipo'].'</td>';
                  if($exibir['tipo'] == "filme")
                  {
                    echo '<td> <a href="'.base_url().'principal/previa/?id='.$exibir['codigo'].'&tipo=filme"> <button class="btn btn-warning"> Prévia</button> </a></td>';
                    echo '<td> <a href="'.base_url().'principal/aprovarsugestao/?id='.$exibir['codigo'].'&tipo=filme&user='.$exibir['user'].'"> <button class="btn btn-success"> Aprovar Sugestão</button> </a></td>';
                    echo '<td> <a href="'.base_url().'principal/excluirsugestao/?id='.$exibir['codigo'].'&tipo=filme&user='.$exibir['user'].'"> <button class="btn btn-danger"> Negar Sugestão</button> </a></td></tr>';
                  }
                  else if($exibir['tipo'] == "jogo")
                  {
                    echo '<td> <a href="'.base_url().'principal/previa/?id='.$exibir['codigo'].'&tipo=jogo"> <button class="btn btn-warning"> Prévia</button> </a></td>';
                    echo '<td> <a href="'.base_url().'principal/aprovarsugestao/?id='.$exibir['codigo'].'&tipo=jogo&user='.$exibir['user'].'"> <button class="btn btn-success"> Aprovar Sugestão</button> </a></td>';
                    echo '<td> <a href="'.base_url().'principal/excluirsugestao/?id='.$exibir['codigo'].'&tipo=jogo&user='.$exibir['user'].'"> <button class="btn btn-danger"> Negar Sugestão</button> </a></td></tr>';
                  }
            }
              echo '</tbody>
            </table>';

          


 	}