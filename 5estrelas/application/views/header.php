<html>
<head>
<meta charset="UTF-8">
    <title><?php echo $title;?></title>
    <link rel="icon" href="<?php echo base_url();?>imagens/icon.png">
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/dashboard.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/navbar.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php base_url() ?>assets/js/bootstrap.min.js"></script>
</head>
<body style="background-color:f8f8f8">

<?php

if(isset($_SESSION['user']))
{

if($_SESSION['user'] != 'convidado')
{
  echo ' <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="'.base_url().'">5 Estrelas</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="'.base_url().'principal/paginausuario"> Meu Perfil</a></li>
            <li><a href="'.base_url().'principal/sair">Sair</a></li>
          </li>
          </ul>
          <form class="navbar-form navbar-right" action="'.base_url().'principal/procurar" method="GET">
            <input type="text" class="form-control" name="procura" placeholder="Procurar..">
            <select class="form-control" name="tipo">         
            <option value="jogo"> Jogo </option>
            <option value="filme"> Filme </option>
            </select>
          </form>
        </div><!--/.nav-collapse -->
      </div>
    </nav>';

}
else
{
  echo ' <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="'.base_url().'">5 Estrelas</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="'.base_url().'principal/paginalogin">Fazer Login</a></li>
            <li><a href="'.base_url().'principal/paginacadastro">Cadastrar</a></li>
          </li>
          </ul>
          <form class="navbar-form navbar-right" action="'.base_url().'principal/procurar" method="GET">
            <input type="text" class="form-control" name="procura" placeholder="Procurar..">
            <select class="form-control" name="tipo">         
            <option value="jogo"> Jogo </option>
            <option value="filme"> Filme </option>
            </select>
          </form>
        </div><!--/.nav-collapse -->
      </div>
    </nav>';
}
}
?>
