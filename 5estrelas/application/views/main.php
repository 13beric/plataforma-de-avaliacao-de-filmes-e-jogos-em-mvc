<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link rel="icon" href="<?php echo base_url();?>imagens/icon.png">
    <title>Bem Vindo ao 5 Estrelas!</title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/carousel.css" rel="stylesheet">
  </head>

<!-- navbar! top -->

  <body>
  <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">5 Estrelas</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="<?php echo base_url().'principal/paginacadastro' ?>">Cadastrar</a></li>
            <li><a href="<?php echo base_url().'principal/convidado' ?>">Navegar como Convidado</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
            <li> <form class="navbar-form navbar-right" method="POST" action="<?php echo base_url();?>principal/login">
            <div class="form-group">
              <input type="text" placeholder="Usuário" name="user" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Senha" name="senha" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Logar</button>
          </form>
          </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>


<!-- carousel !-->
    
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>       
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img class="first-slide" src="<?php base_url() ?>imagens/vingadores.jpg" alt="First slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Bem Vindo ao 5 Estrelas!</h1>
              <p>Criando sua conta você já pode começar a avaliar os seus filmes e jogos preferidos</p>
              <p><a class="btn btn-lg btn-primary" href="<?php base_url(); ?>principal/paginacadastro" role="button">Clique aqui para se cadastrar.</a></p>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="second-slide" src="<?php base_url() ?>imagens/battlefield.jpg" alt="Second slide">
          <div class="container">
            <div class="carousel-caption">
              <h1>Receba recomendações</h1>
              <p>Tenha também acesso às recomendações de outros usuários</p>
              <br><br><br><br>
            </div>
          </div>
        </div>
        
      </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>

