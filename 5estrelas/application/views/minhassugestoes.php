<?php

if(isset($_SESSION['user']))
{
 echo '<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <h2 class="sub-header"> <center> Minhas Sugestôes </center> </h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Título</th>
                  <th>Tipo</th>
                  <th>Estado</th>
                  <th>Excluir</th>
                </tr>
              </thead>
              <tbody>';
              foreach($sugestoes as $exibir)
              {
                echo '<tr>
                  <td>'.wordwrap($exibir['titulo'],16,'<br>',1).'</td>
                  <td>'.$exibir['tipo'].'</td>
                  <td>'.$exibir['estado'].'</td>';
                  if($exibir['tipo'] == "filme")
                  {
                    if($exibir['estado'] == 'pendente')
                    {
                      echo '<td> <a href="'.base_url().'principal/excluirsugestao/?id='.$exibir['codigo'].'&tipo=filme&user='.$_SESSION
                      ['user'].'"> <button class="btn btn-danger"> Excluir Sugestão</button> </a></td>
                        </tr>';
                    }
                  }
                  else if($exibir['tipo'] == "jogo")
                  {
                    if($exibir['estado'] == 'pendente')
                    {
                      echo '<td> <a href="'.base_url().'principal/excluirsugestao/?id='.$exibir['codigo'].'&tipo=jogo&user='.$_SESSION
                      ['user'].'"> <button class="btn btn-danger"> Excluir Sugestão</button> </a></td>
                        </tr>';
                    }
                  }
            }
              echo '</tbody>
            </table>';

          


 	}

?>
