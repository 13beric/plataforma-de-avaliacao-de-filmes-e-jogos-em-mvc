<?php

if(isset($_SESSION['user']))
{
 echo '<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <h2 class="sub-header"> <center> Minhas Avaliações </center> </h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Título</th>
                  <th>Nota</th>
                  <th>Avaliação</th>
                  <th>Alterar</th>
                  <th>Excluir</th>
                </tr>
              </thead>
              <tbody>';
              foreach($avaliacoes as $exibir)
              {
                echo '<tr>
                  <td>'.wordwrap($exibir['titulo'],16,'<br>',1).'</td>
                  <td>'.$exibir['nota'].'</td>
                  <td>'.wordwrap($exibir['avaliacao'],70,'<br>',1).'</td>';
                  if($exibir['tipo'] == "filme")
                  {
                  echo '<td> <a href="'.base_url().'principal/editaravaliacao/?id='.$exibir['codigofilme'].'&tipo=filme"> <button class="btn btn-warning"> Editar</button> </a></td>
                  <td> <a href="'.base_url().'principal/excluiravaliacao/?id='.$exibir['codigofilme'].'&tipo=filme"> <button class="btn btn-danger"> Excluir </button> </a></td>
                    </tr>';
                  }
                  else if($exibir['tipo'] == "jogo")
                  {
                    echo '<td> <a href="'.base_url().'principal/editaravaliacao/?id='.$exibir['codigojogo'].'&tipo=jogo"> <button class="btn btn-warning"> Editar</button> </a></td>
                  <td> <a href="'.base_url().'principal/excluiravaliacao/?id='.$exibir['codigojogo'].'&tipo=jogo"> <button class="btn btn-danger"> Excluir </button> </a></td>
                    </tr>';
                  }
            }
              echo '</tbody>
            </table>';

          


 	}

?>


