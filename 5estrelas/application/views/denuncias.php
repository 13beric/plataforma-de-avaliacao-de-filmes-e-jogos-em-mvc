<?php

if(isset($_SESSION['user']))
{
 echo '<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

        <h2 class="sub-header"> <center> Denúncias </center> </h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Usuário</th>
                  <th>Título</th>
                  <th>Tipo</th>
                  <th>Avaliação</th>
                  <th>Excluir Denúncia</th>
                  <th>Excluir Avaliação</th>
                </tr>
              </thead>
              <tbody>';
              foreach($denuncias as $exibir)
              {
                echo '<tr>
                  <td>'.wordwrap($exibir['usuario'],16,'<br>',1).'</td>
                  <td>'.$exibir['nome_av'].'</td>
                  <td>'.$exibir['tipo'].'</td>
                  <td>'.wordwrap($exibir['avaliacao'],60,'<br>',1).'</td>';
                  if($exibir['tipo'] == "filme")
                  {
                  echo '<td> <a href="'.base_url().'principal/excluirdenuncia/?id='.$exibir['id'].'&tipo=filme&user='.$exibir['usuario'].'"> <button class="btn btn-warning"> Excluir Denúncia </button> </a></td>
                  <td> <a href="'.base_url().'principal/excluiravaliacao/?id='.$exibir['id'].'&tipo=filme&user='.$exibir['usuario'].'"> <button class="btn btn-danger"> Excluir Avaliação </button> </a></td>
                    </tr>';
                  }
                  else if($exibir['tipo'] == "jogo")
                  {
                    echo '<td> <a href="'.base_url().'principal/excluirdenuncia/?id='.$exibir['id'].'&tipo=jogo&user='.$exibir['usuario'].'"> <button class="btn btn-warning"> Excluir Denúncia </button> </a></td>
                  <td> <a href="'.base_url().'principal/excluiravaliacao/?id='.$exibir['id'].'&tipo=jogo&user='.$exibir['usuario'].'"> <button class="btn btn-danger"> Excluir Avaliação </button> </a></td>
                    </tr>';
                  }
                }
             
              echo '</tbody>
            </table>';

 	}
  ?>