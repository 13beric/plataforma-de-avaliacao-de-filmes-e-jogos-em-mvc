<?php

echo '<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="padding:15px; margin:0px; width:100%;">';

foreach($nota as $n)
{
    if($avaliacoes == NULL)
    {
        $nota = "Não há nota";
    }
    else
    {
        $nota = $n['nota'];
    }

}

if(isset($_SESSION['user']))
{
if($_SESSION['user'] == "admin")
 	  {
 			foreach($filme as $exibe)
 			{
 				echo '<div style="height:450px; float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px;">';
                echo '&nbsp; &nbsp; &nbsp; &nbsp; ';
                echo '<h2> <center> '.$exibe['titulo'].'&nbsp; &nbsp; ';
                echo '</center> </h2> <br> <br>';
                echo '<iframe style="vertical-align:top;" width="450" height="260" src="https://www.youtube.com/embed/'.$exibe['video'].'" frameborder="0" allowfullscreen></iframe>';
                echo '&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp <img style="vertical-align:top;" height="260" width="180" src="'.base_url().'cartazes/'.$exibe['cartaz'].'">';
                echo' <div class="col-xs-6 col-sm-3 placeholder">
                <center> <img src="'.base_url().'imagens/estrela.png" width="180" height="180" class="img-responsive" alt="Generic placeholder thumbnail">
                <h3>Nota</h4>
                <h4>'; 
                if($avaliacoes == NULL)
                {
                    echo $nota.'</h5> </center> </div>';
                }
                else
                {
                    echo round($nota,1).'</h5> </center> </div>';
                }
                echo '</div> <div style=" float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px; padding:25px;"> <div style="width:450px; float:right;"> <center> <h4> Informações </h4> </center> <br> Data de Lançamento: '.$exibe['lancamento'].'<br>';
                echo 'Gênero: '.$exibe['genero'].'<br>';
                echo 'Direção: '.$exibe['direcao'].'<br>';
                echo 'Elenco: '.$exibe['elenco'].'<br>';
                echo 'Faixa Etária: '; 

                if($exibe['faixa'] == 0)
                {
                    echo 'Livre';
                }
                else if($exibe['faixa'] <= 16)
                {
                    echo 'Não recomendado para menores de '.$exibe['faixa'].' anos';
                }
                else
                {
                    echo "Proibido para menores de 18 anos";
                }

                echo '</div>';
    			echo '<div style="width:450px; float:left;"> <center> <h4> Sinopse </h4> </center> <br> <p align="justify">';
                echo $exibe['sinopse'].' </p> </div> </div> ';

                echo ' <div style="float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px; padding:25px;"> <center> <h3 class="form-signin-heading">Avaliações: </h3> </center> <br> <br>';

                if($avaliacoes == NULL)
                {
                    echo '<center> <h4 class="form-signin-heading">Este filme não foi avaliado </h4> <br> <br> </center>';
                }
                else
                {
                    foreach($avaliacoes as $listav)
                    {
                        echo '<div  style="border-radius: 10px 10px; background-color:#eee; padding:25px;">';
                        echo '<b>'.strtoupper($listav['usuario']).'</b><br> <br>';
                        echo '<p class="avaliacao" align="justify">'.$listav['avaliacao'].'</p> <br>';
                        echo 'Nota dada: '.$listav['nota'].'<br> <br>';
                        echo '<a href="'.base_url().'principal/excluiravaliacao/?user='.$listav['usuario'].'&id='.$exibe['codigo'].'&tipo=filme"> <button class="btn btn-danger" style="float:right;"> Excluir </button> </a> <br>';
                        echo '</div>';
                        echo '<br>';
                    }
                }

 			}
 		}
        else
        {
            foreach($filme as $exibe)
            {
                echo '<div style="height:450px; float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px;">';
                echo '&nbsp; &nbsp; &nbsp; &nbsp;';
                echo '<h2> <center> '.$exibe['titulo'].'&nbsp; &nbsp; ';
                if($_SESSION['user'] != 'convidado')
                {
                    if($favoritado == 'não')
                    {
                        echo '<a href="'.base_url().'principal/favoritarfilme/?id='.$exibe['codigo'].'"> <button class="btn btn-success"> Favoritar</button> </a>';
                    }
                    else
                    {
                        echo '<a href="'.base_url().'principal/favoritarfilme/?id='.$exibe['codigo'].'"> <button class="btn btn-danger"> Desfavoritar</button> </a>';
                    }
                }
                echo '</center> </h2> <br> <br>';
                echo '<iframe style="vertical-align:top;" width="450" height="260" src="https://www.youtube.com/embed/'.$exibe['video'].'" frameborder="0" allowfullscreen></iframe>';
                echo '&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp <img style="vertical-align:top;" height="260" width="180" src="'.base_url().'cartazes/'.$exibe['cartaz'].'">';
                echo' <div class="col-xs-6 col-sm-3 placeholder">
                <center> <img src="'.base_url().'imagens/estrela.png" width="180" height="180" class="img-responsive" alt="Generic placeholder thumbnail">
                <h3>Nota</h4>
                <h4>'; 
                if($avaliacoes == NULL)
                {
                    echo $nota.'</h5> </center> </div>';
                }
                else
                {
                    echo round($nota,1).'</h5> </center> </div>';
                }
                echo '</div>';


                echo '<div style="float:right; width:250px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px; padding:20px;">';
                echo '<center><b> Baseado nesse filme: </b> <br><br>';
                foreach($parecidos as $exibeparecidos)
                {
                echo '<div style="padding:20px; width:160px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px;">';
                echo '<center><img height="150" width="100" src="'.base_url().'cartazes/'.$exibeparecidos['cartaz'].'"> <br> <br>';
                echo '<b>'.$exibeparecidos['titulo'].'</b> <br> <br>';
                echo '<a href="'.base_url().'principal/exibirfilme/?id='.$exibeparecidos['codigo'].'"> <button class="btn btn-default"> Visualizar </button> </a>';
                echo '</center> </div> <br>';
                }                       
                echo '</div>';


                echo '<div style=" float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px; padding:25px;"> <div style="width:450px; float:right;"> 
                <center> 
                <h4> Informações </h4> 
                </center> 
                <br> Data de Lançamento: '.$exibe['lancamento'].'<br>';
                echo 'Gênero: '.$exibe['genero'].'<br>';
                echo 'Direção: '.$exibe['direcao'].'<br>';
                echo 'Elenco: '.$exibe['elenco'].'<br>';
                echo 'Faixa Etária: '; 

                if($exibe['faixa'] == 0)
                {
                    echo 'Livre';
                }
                else if($exibe['faixa'] <= 16)
                {
                    echo 'Não recomendado para menores de '.$exibe['faixa'].' anos';
                }
                else
                {
                    echo "Proibido para menores de 18 anos";
                }

                echo '</div>';
                echo '<div style="width:450px; float:left;"> 
                <center> 
                <h4> Sinopse </h4> 
                </center> 
                <br> 
                <p align="justify">';
                echo $exibe['sinopse'].' </p> </div> </div> ';

                if($_SESSION['user'] != 'convidado')
                {
                if($avaliado =='não')
                {
                    echo '<div style="float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px; padding:25px;"> <center> <h3 class="form-signin-heading">Faça sua avaliação: </h3> </center> <br>';
                    echo '<form action="'.base_url().'principal/avaliar/?id='.$id.'&tipo=filme" method="POST">';
                    echo '<textarea style="height: 150px;" id="inputAvaliacao" class="form-control" placeholder="Digite aqui sua avaliação" name="avaliacao" required></textarea> <br>';
                    echo 'Nota: <select class="form-control" style="width:80px;" name=nota>
                    <option value="0"> 0 </option>
                    <option value="1"> 1 </option>
                    <option value="2"> 2 </option>
                    <option value="3"> 3 </option>
                    <option value="4"> 4 </option>
                    <option value="5"> 5 </option>
                    </select>';
                    echo '<center> <button class="btn btn-lg btn-primary btn-block" style="width:200px; float:right;" type="submit">Enviar Avaliação</button> </center> </form> <br> <br> <br> </div>'; 
                }
                }
                else
                {
                    echo ' <div style="float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px; padding:25px;"><center> <a href="'.base_url().'principal/paginalogin"> <h2>Faça login para avaliar </h2> </a> </center> <br> <br> </div>';
                }

                echo ' <div style="float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px; padding:25px;"> <center> <h3 class="form-signin-heading">Avaliações: </h3> </center> <br> <br>';

                if($avaliacoes == NULL)
                {
                    echo '<center> <h4 class="form-signin-heading">Este filme não foi avaliado </h4> <br> <br> </center>';
                }
                else
                {
                    foreach($avaliacoes as $listav)
                    {
                        echo '<div  style="border-radius: 10px 10px; background-color:#eee; padding:25px;">';
                        echo '<b>'.strtoupper($listav['usuario']).'</b><br> <br>';
                        echo '<p class="avaliacao" align="justify">'.$listav['avaliacao'].'</p> <br>';
                        echo 'Nota dada: '.$listav['nota'].'<br> <br>';
                        if($listav['usuario'] == $_SESSION['user'])
                        {
                            echo '<td> <a href="'.base_url().'principal/editaravaliacao/?id='.$exibe['codigo'].'&tipo=filme"> <button class="btn btn-warning" > Editar</button> </a>';
                            echo '<a href="'.base_url().'principal/excluiravaliacao/?id='.$exibe['codigo'].'&tipo=filme"> <button class="btn btn-danger" style="float:right;"> Excluir </button> </a> <br>';
                        }
                        else if($_SESSION['user'] != 'convidado')
                        {
                            echo '<a href="'.base_url().'principal/denunciar/?id='.$exibe['codigo'].'&tipo=filme&user='.$listav['usuario'].'"> <button class="btn btn-danger" style="float:right;"> Denunciar </button> </a> <br>';
                        }
                        echo '</div>';
                        echo "<br>";
                    }
                    echo '</div>';
                }
            }
        }
    }
 	

?>