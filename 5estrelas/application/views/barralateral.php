    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">

    <?php 
if(isset($_SESSION['user']))
{
    if($_SESSION['user']=='admin')
		  {
        if($title == "Menu do Usuário")
        {
			  echo '<li class="active"><a href="'.base_url().'principal/paginausuario">Menu do Usuário <span class="sr-only">(current)</span></a></li>
            <li><a href="'.base_url().'principal/adicionar">Adicionar Filme/Jogo</a></li>
            <li><a href="'.base_url().'principal/listarfilmes">Gerenciar Filmes</a></li>
            <li><a href="'.base_url().'principal/listarjogos">Gerenciar Jogos</a></li>
            <li><a href="'.base_url().'principal/listarusers">Gerenciar Usuários</a></li>
            <li><a href="'.base_url().'principal/paginadenuncias">Denúncias</a></li> 
            <li><a href="'.base_url().'principal/sugestoes">Sugestões</a></li> </ul> </div>';
          }
          else if($title == "Adicionar")
          {
            echo '<li><a href="'.base_url().'principal/paginausuario">Menu do Usuário </a></li>
            <li class="active"><a href="'.base_url().'principal/adicionar">Adicionar Filme/Jogo <span class="sr-only">(current)</span> </a></li>
            <li><a href="'.base_url().'principal/listarfilmes">Gerenciar Filmes</a></li>
            <li><a href="'.base_url().'principal/listarjogos">Gerenciar Jogos</a></li>
            <li><a href="'.base_url().'principal/listarusers">Gerenciar Usuários</a></li>
            <li><a href="'.base_url().'principal/paginadenuncias">Denúncias</a></li> 
            <li><a href="'.base_url().'principal/sugestoes">Sugestões</a></li></ul> 
            </div>';
          }
          else if($title == "Filmes")
          {
            echo '<li><a href="'.base_url().'principal/paginausuario">Menu do Usuário </a></li>
            <li><a href="'.base_url().'principal/adicionar">Adicionar Filme/Jogo</a></li>
            <li class="active"><a href="'.base_url().'principal/listarfilmes">Gerenciar Filmes <span class="sr-only">(current)</span> </a></li>
            <li><a href="'.base_url().'principal/listarjogos">Gerenciar Jogos</a></li>
            <li><a href="'.base_url().'principal/listarusers">Gerenciar Usuários</a></li>
            <li><a href="'.base_url().'principal/paginadenuncias">Denúncias</a></li> 
            <li><a href="'.base_url().'principal/sugestoes">Sugestões</a></li></ul> </div>';
          }
          else if($title == "Jogos")
          {
            echo '<li><a href="'.base_url().'principal/paginausuario">Menu do Usuário </a></li>
            <li><a href="'.base_url().'principal/adicionar">Adicionar Filme/Jogo</a></li>
            <li><a href="'.base_url().'principal/listarfilmes">Gerenciar Filmes</a></li>
            <li class="active"><a href="'.base_url().'principal/listarjogos">Gerenciar Jogos <span class="sr-only">(current)</span></a></li>
            <li><a href="'.base_url().'principal/listarusers">Gerenciar Usuários</a></li>
            <li><a href="'.base_url().'principal/paginadenuncias">Denúncias</a></li> 
            <li><a href="'.base_url().'principal/sugestoes">Sugestões</a></li></ul> </div>';
          }
          else if($title == "Usuários")
          {
            echo '<li><a href="'.base_url().'principal/paginausuario">Menu do Usuário </a></li>
            <li><a href="'.base_url().'principal/adicionar">Adicionar Filme/Jogo</a></li>
            <li><a href="'.base_url().'principal/listarfilmes">Gerenciar Filmes</a></li>
            <li><a href="'.base_url().'principal/listarjogos">Gerenciar Jogos</a></li>
            <li class="active"><a href="'.base_url().'principal/listarusers">Gerenciar Usuários <span class="sr-only">(current)</span></a></li>
            <li><a href="'.base_url().'principal/paginadenuncias">Denúncias</a></li> 
            <li><a href="'.base_url().'principal/sugestoes">Sugestões</a></li></ul> </div>';
          }
          else if($title == 'Denúncias')
          {
            echo '<li><a href="'.base_url().'principal/paginausuario">Menu do Usuário </a></li>
            <li><a href="'.base_url().'principal/adicionar">Adicionar Filme/Jogo</a></li>
            <li><a href="'.base_url().'principal/listarfilmes">Gerenciar Filmes</a></li>
            <li><a href="'.base_url().'principal/listarjogos">Gerenciar Jogos</a></li>
            <li><a href="'.base_url().'principal/listarusers">Gerenciar Usuários</a></li>
            <li class="active"><a href="'.base_url().'principal/paginadenuncias">Denúncias <span class="sr-only">(current)</span> </a></li> 
            <li><a href="'.base_url().'principal/sugestoes">Sugestões</a></li></ul> </div>';
          }
          else
          {
            echo '<li><a href="'.base_url().'principal/paginausuario">Menu do Usuário </a></li>
            <li><a href="'.base_url().'principal/adicionar">Adicionar Filme/Jogo</a></li>
            <li><a href="'.base_url().'principal/listarfilmes">Gerenciar Filmes</a></li>
            <li><a href="'.base_url().'principal/listarjogos">Gerenciar Jogos</a></li>
            <li><a href="'.base_url().'principal/listarusers">Gerenciar Usuários</a></li>
            <li><a href="'.base_url().'principal/paginadenuncias">Denúncias </a></li> 
            <li class="active"><a href="'.base_url().'principal/sugestoes">Sugestões <span class="sr-only">(current)</span> </a></li> </ul> </div>';
          }
      }
      else if($_SESSION['user'] != 'convidado')
      {
        if($title == "Menu do Usuário")
        {
        echo '<li class="active"><a href="'.base_url().'principal/paginausuario">Menu do Usuário <span class="sr-only">(current)</span></a></li>
            <li><a href="'.base_url().'principal/minhasavaliacoes">Minhas avaliações</a></li></ul>
            <ul class="nav nav-sidebar">
            <li><a href="'.base_url().'principal/sugerir">Sugerir</a></li>
            <li><a href="'.base_url().'principal/minhassugestoes">Minhas Sugestôes</a></li></ul> 
            </div>';
          }
          else if($title == "Minhas avaliações")
          {
            echo '<li><a href="'.base_url().'principal/paginausuario">Menu do Usuário</a></li>
            <li class="active"><a href="'.base_url().'principal/minhasavaliacoes">Minhas avaliações <span class="sr-only">(current)</span></a></li></ul>
            <ul class="nav nav-sidebar">
            <li><a href="'.base_url().'principal/sugerir">Sugerir</a></li>
            <li><a href="'.base_url().'principal/minhassugestoes">Minhas Sugestôes</a></li></ul> 
            </div>';
          }
          else if($title == 'Sugerir')
          {
            echo '<li><a href="'.base_url().'principal/paginausuario">Menu do Usuário </a></li>
            <li><a href="'.base_url().'principal/minhasavaliacoes">Minhas avaliações</a></li></ul>
            <ul class="nav nav-sidebar">
            <li class="active"><a href="'.base_url().'principal/sugerir">Sugerir <span class="sr-only">(current)</span></a></li> 
            <li><a href="'.base_url().'principal/minhassugestoes">Minhas Sugestôes</a></li></ul> 
            </div>';
          }
          else if($title == "Minhas Sugestões")
          {
            echo '<li><a href="'.base_url().'principal/paginausuario">Menu do Usuário </a></li>
            <li><a href="'.base_url().'principal/minhasavaliacoes">Minhas avaliações</a></li></ul>
            <ul class="nav nav-sidebar">
            <li><a href="'.base_url().'principal/sugerir">Sugerir</a></li> 
            <li class="active"><a href="'.base_url().'principal/minhassugestoes">Minhas Sugestões <span class="sr-only">(current)</span></a></li></ul> 
            </div>';
          }
          else
          {
            echo '<li><a href="'.base_url().'principal/paginausuario">Menu do Usuário </a></li>
            <li><a href="'.base_url().'principal/minhasavaliacoes">Minhas avaliações</a></li></ul>
            <ul class="nav nav-sidebar">
            <li><a href="'.base_url().'principal/sugerir">Sugerir</a></li> 
            <li><a href="'.base_url().'principal/minhassugestoes">Minhas Sugestões</a></li></ul> 
            </div>';
          }
      }
}

      ?>

      </ul>