<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <link rel="icon" href="<?php echo base_url();?>imagens/icon.png">
    <title>Cadastro</title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/signin.css" rel="stylesheet">
  </head>

  <body>

<!-- navbar top !-->

    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url() ?>"> 5 Estrelas</a>
        </div>
      </div>
    </nav>

<!-- formulário !-->

    <div class="container">

      <form class="form-signin" method="POST" action="<?php echo base_url(); ?>principal/cadastrar">
        <br>
        <br>
        <br>
        <center> <h2 class="form-signin-heading">Faça seu cadastro</h2></center>
        <label for="inputUser" class="sr-only">Usuário</label>
        <input type="text" id="inputUser" class="form-control" placeholder="Usuário" name="user" required>
        <label for="inputPassword" class="sr-only">Senha</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Senha" name="senha" required>      
        <button class="btn btn-lg btn-primary btn-block" type="submit">Cadastrar</button> 
      </form>
      <center><p><a class="btn btn-lg btn-success" href="<?php base_url(); ?>paginalogin" role="button">Clique aqui para logar</a></p></center>

      </div>

  