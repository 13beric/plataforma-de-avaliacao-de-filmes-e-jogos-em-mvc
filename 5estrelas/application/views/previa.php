<?php

echo '<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="padding:15px; margin:0px; width:100%;">';

if($tipo == 'Filme')
{
	foreach($filme as $exibe)
 		{
 				echo '<div style="height:450px; float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px;">';
                echo '&nbsp; &nbsp; &nbsp; &nbsp; ';
                echo '<h2> <center> '.$exibe['titulo'].'&nbsp; &nbsp; 
                <a href="'.base_url().'principal/aprovarsugestao/?id='.$exibe['codigo'].'&tipo=filme&user='.$exibe['user'].'"> <button class="btn btn-success"> Aprovar Sugestão</button> </a> 
                <a href="'.base_url().'principal/excluirsugestao/?id='.$exibe['codigo'].'&tipo=filme&user='.$exibe['user'].'"> <button class="btn btn-danger"> Negar Sugestão</button> </a> ';
                echo '</center> </h2> <br> <br>';
                echo '<center><iframe style="vertical-align:top;" width="450" height="260" src="https://www.youtube.com/embed/'.$exibe['video'].'" frameborder="0" allowfullscreen></iframe>';
                echo '&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp <img style="vertical-align:top;" height="260" width="180" src="'.base_url().'cartazes/'.$exibe['cartaz'].'">';
                echo ' </center> </div> <div style=" float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px; padding:25px;"> <div style="width:450px; float:right;"> <center> <h4> Informações </h4> </center> <br> Data de Lançamento: '.$exibe['lancamento'].'<br>';
                echo 'Gênero: '.$exibe['genero'].'<br>';
                echo 'Direção: '.$exibe['direcao'].'<br>';
                echo 'Elenco: '.$exibe['elenco'].'<br>';
                echo 'Faixa Etária: '; 

                if($exibe['faixa'] == 0)
                {
                    echo 'Livre';
                }
                else if($exibe['faixa'] <= 16)
                {
                    echo 'Não recomendado para menores de '.$exibe['faixa'].' anos';
                }
                else
                {
                    echo "Proibido para menores de 18 anos";
                }

                echo '</div>';
    			echo '<div style="width:450px; float:left;"> <center> <h4> Sinopse </h4> </center> <br> <p align="justify">';
                echo $exibe['sinopse'].' </p> </div> </div> ';

 			}
}
else if($tipo == 'Jogo')
{
	foreach($jogo as $exibe)
 	{
            echo '<div style="height:450px; float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px;">';
    		echo '&nbsp; &nbsp; &nbsp; &nbsp; ';
            echo '<h2> <center> '.$exibe['titulo'].'&nbsp; &nbsp; 
            <a href="'.base_url().'principal/aprovarsugestao/?id='.$exibe['codigo'].'&tipo=jogo&user='.$exibe['user'].'"> <button class="btn btn-success"> Aprovar Sugestão</button> </a>
            <a href="'.base_url().'principal/excluirsugestao/?id='.$exibe['codigo'].'&tipo=jogo&user='.$exibe['user'].'"> <button class="btn btn-danger"> Negar Sugestão</button> </a>';
            echo '</center> </h2> <br> <br>';
            echo '<center><iframe style="vertical-align:top;" width="450" height="260" src="https://www.youtube.com/embed/'.$exibe['video'].'" frameborder="0" allowfullscreen></iframe>';
            echo '&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp <img style="vertical-align:top;" height="260" width="180" src="'.base_url().'cartazes/'.$exibe['cartaz'].'">';
            echo '</center></div> <div style=" float:left; width:1000px; border:1px solid #e7e7e7; background-color:#FFF; border-radius: 10px 10px; margin:8px; padding:25px;"><center> <h4> Informações </h4> </center> <br> Data de Lançamento: '.$exibe['lancamento'].'<br>';
    		echo 'Plataformas: '.$exibe['plataformas'].'<br>';
    		echo 'Gênero: '.$exibe['genero'].'<br>';
    		echo 'Lançamento: '.$exibe['lancamento'].'<br>';
    		echo 'Faixa Etária: '; 
    			  if($exibe['faixa'] == 0)
            {
              echo 'Livre';
            }
            else
            {
              echo 'Não recomendado para menores de '.$exibe['faixa'].' anos';
            }
    		echo '</div>';
    	}
}

?>