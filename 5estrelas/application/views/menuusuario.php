    <?php 

    if($_SESSION['user']=='admin')
		  {

        echo '<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <br> <br>
          <center> <h1 class="page-header">Bem Vindo, Administrador</h1> </center>
          <br> <br>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="'.base_url().'imagens/controle.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Jogos Cadastrados</h4>
              <span class="text-muted">'.$contjogo.'</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="'.base_url().'imagens/claquete2.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Filmes Cadastrados</h4>
              <span class="text-muted">'.$contfilme.'</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="'.base_url().'imagens/pessoa.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Usuários Cadastrados</h4>
              <span class="text-muted">'.$contuser.'</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="'.base_url().'imagens/avaliacao.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Total de Avaliações</h4>
              <span class="text-muted">'.$contav.'</span>
            </div>
          </div>
        </div>
      </div>
    </div>';
		  }
		  else
		  {
        echo '<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <br> <br>
          <center> <h1 class="page-header">Bem vindo, '.$_SESSION['user'].'</h1> </center>
          <br> <br>

          <div class="row placeholders">
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="'.base_url().'imagens/claquete2.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Filmes avaliados por você</h4>
              <span class="text-muted">'.$avfilmes.'</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="'.base_url().'imagens/controle.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Jogos avaliados por você</h4>
              <span class="text-muted">'.$avjogos.'</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="'.base_url().'imagens/mouse.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Número de acessos</h4>
              <span class="text-muted">'.$qntacessos.'</span>
            </div>
            <div class="col-xs-6 col-sm-3 placeholder">
              <img src="'.base_url().'imagens/denuncia.png" width="200" height="200" class="img-responsive" alt="Generic placeholder thumbnail">
              <h4>Número de denúncias</h4>
              <span class="text-muted">'.$qntdenuncias.'</span>
            </div>
          </div>

      
        </div>
    </div>';
		  }
      ?>