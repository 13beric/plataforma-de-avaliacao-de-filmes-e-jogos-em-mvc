<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Principal extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$array = array('title'=>'Início');
		$this->load->view('header',$array);
		$this->load->view('navegar');
	}
	public function paginacadastro()
	{
		$this->load->view('cadastro');
	}
	public function login()
	{
		$this->load->model('usuario');
		$this->usuario->construtor($_POST['user'],$_POST['senha']);
        $this->usuario->logar();
	}
	public function cadastrar()
	{
		$this->load->model('usuario');
		$this->usuario->construtor($_POST['user'],$_POST['senha']);
        $this->usuario->cadastrar();
	}
	public function paginalogin()
	{
		$this->load->view('login');
	}
	public function paginausuario()
	{
		$this->load->model('filme');
		$this->load->model('jogo');
		$this->load->model('usuario');	
		if($_SESSION['user'] == 'admin')
		{
			$contfilme = $this->filme->getCont();
			$contjogo = $this->jogo->getCont();
			$contuser = $this->usuario->getCont();
			$contav = $this->usuario->getQntAv();
			$array = array('title'=>"Menu do Usuário", 'contfilme'=>$contfilme, 'contjogo'=>$contjogo, 'contuser'=>$contuser, 'contav'=>$contav);
			$this->load->view('header',$array);
			$this->load->view('barralateral',$array);
			$this->load->view('menuusuario', $array);
		}
		else
		{
			$avfilmes = $this->filme->getContUser();
			$avjogos = $this->jogo->getContUser();
			$qntdenuncias = $this->usuario->getContDenuncias();
			$qntacessos = $this->usuario->getAcessos();
			$array = array('title'=>"Menu do Usuário", 'avfilmes'=>$avfilmes, 'avjogos'=>$avjogos, 'qntdenuncias'=>$qntdenuncias, 'qntacessos'=>$qntacessos);
			$this->load->view('header',$array);
			$this->load->view('barralateral',$array);
			$this->load->view('menuusuario', $array);
		}
	}
	public function adicionar()
	{
		$array = array('title'=>"Adicionar");
		$this->load->view('header',$array);
		$this->load->view('barralateral',$array);
		if(null !== $this->input->get('classe'))
		{		
			if($this->input->get('classe') == "filme")
			{
				$classe = array('classe'=>"filme");
			}
			else if($this->input->get('classe') == "jogo")
			{
				$classe = array('classe'=>"jogo");
			}
			$this->load->view('adicionar',$classe);
		}
		else
		{
			$this->load->view('adicionar');
		}
	}
	public function cadastrarfilme()
	{
		$this->load->model('filme');
		$this->filme->construtor($_POST['titulo'],$_POST['lancamento'],$_POST['genero'],$_POST['elenco'],$_POST['direcao'],$_POST['video'],$_FILES['cartaz'],$_POST['faixa'],$_POST['sinopse']);
		$this->filme->cadastrar();
	}
	public function cadastrarjogo()
	{
		$this->load->model('jogo');
		$this->jogo->construtor($_POST['titulo'],$_POST['estudio'],$_POST['lancamento'],$_POST['plataformas'],$_POST['genero'],$_POST['faixa'],$_POST['video'],$_FILES['cartaz']);
		$this->jogo->cadastrar();
	}
	public function listarfilmes()
	{
		$this->load->model('filme');
		$array = array('title'=>"Filmes", 'filmes'=>$this->filme->listarfilmes());
		$this->load->view('header',$array);
		$this->load->view('barralateral',$array);
		$this->load->view('listarfilmes',$array);
	}
	public function exibirfilme()
	{
		$this->load->model('filme');
		$avaliacoes = $this->filme->getAvaliacoes($this->input->get('id'));
		$nota = $this->filme->getNota($this->input->get('id'));
		if($_SESSION['user'] == "admin")
		{
			$dadosfilme = $this->filme->getDadosFilme($this->input->get('id'));
			$array = array('title'=>"Filmes", 'id'=>$this->input->get('id'), 'filme'=>$dadosfilme, 'avaliacoes'=>$avaliacoes, 'nota'=>$nota);
			$this->load->view('header',$array);
			$this->load->view('exibefilme',$array);
		}
		else
		{
			$this->load->model('usuario');
			$existeav = $this->usuario->verificaAvaliacao($_SESSION['user'],"filmes",$this->input->get('id'));
			$filmesparecidos = $this->filme->parecidos($this->input->get('id'));
			if($existeav == 1)
			{
				$dadosfilme = $this->filme->getDadosFilme($this->input->get('id'));
				$favoritado = $this->filme->verificarfavorito($this->input->get('id'));
				$array = array('title'=>"Filmes", 'id'=>$this->input->get('id'), 'filme'=>$dadosfilme, 'avaliacoes'=>$avaliacoes, 'avaliado'=>"sim",'nota'=>$nota, 'favoritado'=>$favoritado, 'parecidos'=>$filmesparecidos);
				$this->load->view('header',$array);
				$this->load->view('exibefilme',$array);
			}
			else
			{
				$dadosfilme = $this->filme->getDadosFilme($this->input->get('id'));
				$favoritado = $this->filme->verificarfavorito($this->input->get('id'));
				$array = array('title'=>"Filmes", 'id'=>$this->input->get('id'), 'filme'=>$dadosfilme, 'avaliacoes'=>$avaliacoes, 'avaliado'=>"não", 'nota'=>$nota, 'favoritado'=>$favoritado, 'parecidos'=>$filmesparecidos);
				$this->load->view('header',$array);
				$this->load->view('exibefilme',$array);
			}
		}
	}
	
	public function listarjogos()
	{
		$this->load->model('jogo');
		$array = array('title'=>"Jogos", 'jogos'=>$this->jogo->listarjogos());
		$this->load->view('header',$array);
		$this->load->view('barralateral',$array);
		$this->load->view('listarjogos',$array);
	}
	public function exibirjogo()
	{
		$this->load->model('jogo');
		$avaliacoes = $this->jogo->getAvaliacoes($this->input->get('id'));
		$nota = $this->jogo->getNota($this->input->get('id'));
		if($_SESSION['user'] == "admin")
		{
			$dadosjogo = $this->jogo->getDadosJogo($this->input->get('id'));
			$array = array('title'=>"Jogos", 'id'=>$this->input->get('id'), 'jogo'=>$dadosjogo, 'avaliacoes'=>$avaliacoes,'nota'=>$nota);
			$this->load->view('header',$array);
			$this->load->view('exibejogo',$array);
		}
		else
		{
			$this->load->model('usuario');
			$existeav = $this->usuario->verificaAvaliacao($_SESSION['user'],"jogos",$this->input->get('id'));
			$jogosparecidos = $this->jogo->parecidos($this->input->get('id'));
			if($existeav == 1)
			{
				$dadosjogo = $this->jogo->getDadosJogo($this->input->get('id'));
				$favoritado = $this->jogo->verificarfavorito($this->input->get('id'));
				$array = array('title'=>"Jogos", 'id'=>$this->input->get('id'), 'jogo'=>$dadosjogo, 'avaliacoes'=>$avaliacoes, 'avaliado'=>"sim",'nota'=>$nota, 'favoritado'=>$favoritado, 'parecidos'=>$jogosparecidos);
				$this->load->view('header',$array);
				$this->load->view('exibejogo',$array);
			}
			else
			{
				$dadosjogo = $this->jogo->getDadosJogo($this->input->get('id'));
				$favoritado = $this->jogo->verificarfavorito($this->input->get('id'));
				$array = array('title'=>"Jogos", 'id'=>$this->input->get('id'), 'jogo'=>$dadosjogo, 'avaliacoes'=>$avaliacoes, 'avaliado'=>"não",'nota'=>$nota, 'favoritado'=>$favoritado, 'parecidos'=>$jogosparecidos);
				$this->load->view('header',$array);
				$this->load->view('exibejogo',$array);
			}
		}
	}
	public function sair()
	{
		$this->load->model('usuario');
		$this->usuario->sair();	
	}
	public function excluirfilme()
	{
		$this->load->model('filme');
		$this->filme->excluir($this->input->get('id'));
	}
	public function excluirjogo()
	{
		$this->load->model('jogo');
		$this->jogo->excluir($this->input->get('id'));
	}
	public function procurar()
	{
		if($this->input->get('tipo') == "jogo")
		{
			$this->load->model('jogo');
			$resultpesquisa = $this->jogo->busca($this->input->get('procura'));
			$array = array('title'=>"Jogos", 'jogos'=>$resultpesquisa);
			$this->load->view('header',$array);
			$this->load->view('listarjogos',$array);

		}
		else if($this->input->get('tipo') == "filme")
		{
			$this->load->model('filme');
			$resultpesquisa = $this->filme->busca($this->input->get('procura'));
			$array = array('title'=>"Filmes", 'filmes'=>$resultpesquisa);
			$this->load->view('header',$array);
			$this->load->view('listarfilmes');
		}
	}

	public function avaliar()
	{
		$this->load->model('usuario');
		$this->usuario->avaliar($_SESSION['user'],$this->input->get('tipo'),$this->input->get('id'),$_POST['avaliacao'], $_POST['nota']);
	}

	public function excluiravaliacao()
	{
		$this->load->model('usuario');
		if($_SESSION['user'] == "admin")
		{
			$this->usuario->excluiravaliacao($this->input->get('user'),$this->input->get('tipo'),$this->input->get('id'));
		}
		else
		{
			$this->usuario->excluiravaliacao($_SESSION['user'],$this->input->get('tipo'),$this->input->get('id'));
		}
	}

	public function navegar()
	{
		if(null !== $this->input->get('tipo'))
		{
			$tipo = $this->input->get('tipo');

			if($tipo == 'filme')
				{
					if(null !== $this->input->get('categoria'))
					{
						$this->load->model('filme');
						$conteudo = $this->filme->navegar($this->input->get('order'),$this->input->get('categoria'));
						$categorias = $this->filme->getCategorias();
						if($_SESSION['user'] != 'convidado')
						{
							$this->load->model('usuario');
							$recomendacoes = $this->usuario->recomendacao('filme');
							$array = array('title'=>"Navegar", 'conteudo'=>$conteudo, 'categorias'=>$categorias,'recomendacoes'=>$recomendacoes);
						}
						else
						{
							$array = array('title'=>"Navegar", 'conteudo'=>$conteudo, 'categorias'=>$categorias);
						}
						$this->load->view('header',$array);
						$this->load->view('navegar',$array);
					}		
					else
					{
						$this->load->model('filme');
						$conteudo = $this->filme->navegar($this->input->get('order'),'não');
						$categorias = $this->filme->getCategorias();
						if($_SESSION['user'] != 'convidado')
						{
							$this->load->model('usuario');
							$recomendacoes = $this->usuario->recomendacao('filme');
							$array = array('title'=>"Navegar", 'conteudo'=>$conteudo, 'categorias'=>$categorias,'recomendacoes'=>$recomendacoes);
						}
						else
						{
							$array = array('title'=>"Navegar", 'conteudo'=>$conteudo, 'categorias'=>$categorias);
							$this->load->view('header',$array);
						}
						$this->load->view('header',$array);
						$this->load->view('navegar',$array);

					}		
				}
				
				else if($tipo == 'jogo')
				{
					if(null !== $this->input->get('categoria'))
					{
						$this->load->model('jogo');
						$conteudo = $this->jogo->navegar($this->input->get('order'), $this->input->get('categoria'));
						$categorias = $this->jogo->getCategorias();
						if($_SESSION['user'] != 'convidado')
						{
							$this->load->model('usuario');
							$recomendacoes = $this->usuario->recomendacao('jogo');
							$array = array('title'=>"Navegar", 'conteudo'=>$conteudo, 'categorias'=>$categorias,'recomendacoes'=>$recomendacoes);
						}
						else
						{
							$array = array('title'=>"Navegar", 'conteudo'=>$conteudo, 'categorias'=>$categorias);
						}
						$this->load->view('header',$array);
						$this->load->view('navegar',$array);
					}
					else
					{
						$this->load->model('jogo');
						$conteudo = $this->jogo->navegar($this->input->get('order'),'não');
						$categorias = $this->jogo->getCategorias();
						if($_SESSION['user'] != 'convidado')
						{
							$this->load->model('usuario');
							$recomendacoes = $this->usuario->recomendacao('jogo');
							$array = array('title'=>"Navegar", 'conteudo'=>$conteudo, 'categorias'=>$categorias,'recomendacoes'=>$recomendacoes);
						}
						else
						{
							$array = array('title'=>"Navegar", 'conteudo'=>$conteudo, 'categorias'=>$categorias);
						}
						$this->load->view('header',$array);
						$this->load->view('navegar',$array);
					}

				}
		}
		else
		{
			$array1 = array('title'=>"Navegar");
			$this->load->view('header',$array1);
			$this->load->view('navegar',$array1);
		}
	}

	public function minhasavaliacoes()
	{
		$this->load->model('usuario');
		$avaliacoes = $this->usuario->getAvaliacoes();
		$array = array('title'=>"Minhas avaliações", 'avaliacoes'=>$avaliacoes);
		$this->load->view('header',$array);
		$this->load->view('barralateral',$array);
		$this->load->view('myavaliacoes',$array);
	}

	public function editaravaliacao()
	{
		$this->load->model('usuario');
		$avaliacao = $this->usuario->getAvaliacao($this->input->get('id'),$this->input->get('tipo'));
		$array = array('title'=>"Editar Avaliação", 'id'=>$this->input->get('id'), 'tipo'=>$this->input->get('tipo'), 'avaliacao'=>$avaliacao);
		$this->load->view('header',$array);
		$this->load->view('barralateral',$array);
		$this->load->view('editaravaliacao',$array);
	}

	public function atualizaravaliacao()
	{
		$this->load->model('usuario');
		$this->usuario->editarAvaliacao($this->input->get('id'),$this->input->get('tipo'),$_POST['avaliacao'],$_POST['nota']);
	}

	public function denunciar()
	{
		$this->load->model('usuario');
		$this->usuario->denunciar($this->input->get('id'), $this->input->get('tipo'), $this->input->get('user'));
	}

	public function paginaDenuncias()
	{
		$this->load->model('usuario');
		$denuncias = $this->usuario->getDenuncias();
		$array = array('title'=>'Denúncias','denuncias'=>$denuncias);
		$this->load->view('header',$array);
		$this->load->view('barralateral',$array);
		$this->load->view('denuncias',$array);
	}

	public function excluirdenuncia()
	{
		$this->load->model('usuario');
		$this->usuario->excluirdenuncia($this->input->get('id'),$this->input->get('tipo'),$this->input->get('user'));
	}
	public function listarusers()
	{
		$this->load->model('usuario');
		$usuarios = $this->usuario->listarusers();
		$array = array('title'=>'Usuários','usuarios'=>$usuarios);
		$this->load->view('header',$array);
		$this->load->view('barralateral',$array);
		$this->load->view('usuarios',$array);
	}
	public function excluiruser()
	{
		$this->load->model('usuario');
		$this->usuario->excluirusuario($this->input->get('id'));
	}
	public function favoritarfilme()
	{
		$this->load->model('filme');
		$this->filme->favoritar($this->input->get('id'));
	}
	public function favoritarjogo()
	{
		$this->load->model('jogo');
		$this->jogo->favoritar($this->input->get('id'));
	}
	public function recomendar()
	{
		$this->load->model('usuario');
		$this->usuario->recomendacao("jogo");
	}
	public function sugestoes()
	{
		$this->load->model('usuario');
		$sugestoes = $this->usuario->getSugestoes();
		$array = array('title'=>'Sugestôes','sugestoes'=>$sugestoes);
		$this->load->view('header',$array);
		$this->load->view('barralateral',$array);
		$this->load->view('sugestoes',$array);
	}
	public function sugerir()
	{
		$array = array('title'=>'Sugerir');
		$this->load->view('header',$array);
		$this->load->view('barralateral',$array);
		if(null !== $this->input->get('classe'))
		{		
			if($this->input->get('classe') == "filme")
			{
				$classe = array('classe'=>"filme");
			}
			else if($this->input->get('classe') == "jogo")
			{
				$classe = array('classe'=>"jogo");
			}
			$this->load->view('sugerir',$classe);
		}
		else
		{
			$this->load->view('sugerir');
		}
	}
	public function minhassugestoes()
	{
		$this->load->model('usuario');
		$sugestoes = $this->usuario->minhassugestoes();
		$array = array('title'=>'Minhas Sugestões','sugestoes'=>$sugestoes);
		$this->load->view('header',$array);
		$this->load->view('barralateral',$array);
		$this->load->view('minhassugestoes',$array);
	}
	public function enviarsugestao()
	{
		if($this->input->get('tipo') == "filme")
		{
			$this->load->model('filme');
			$this->filme->construtor($_POST['titulo'],$_POST['lancamento'],$_POST['genero'],$_POST['elenco'],$_POST['direcao'],$_POST['video'],$_FILES['cartaz'],$_POST['faixa'],$_POST['sinopse']);
			$this->filme->enviarsugestao();
		}
		else if($this->input->get('tipo') == "jogo")
		{
			$this->load->model('jogo');
			$this->jogo->construtor($_POST['titulo'],$_POST['estudio'],$_POST['lancamento'],$_POST['plataformas'],$_POST['genero'],$_POST['faixa'],$_POST['video'],$_FILES['cartaz']);
			$this->jogo->enviarsugestao();
		}
	}

	public function previa()
	{
		if($this->input->get('tipo') == "filme")
		{
			$this->load->model('filme');
			$dadosfilme = $this->filme->getPrevia($this->input->get('id'));
			$array = array('title'=>"Prévia", 'id'=>$this->input->get('id'), 'filme'=>$dadosfilme, 'tipo'=>'Filme');
			$this->load->view('header',$array);
			$this->load->view('previa',$array);
		}
		else if($this->input->get('tipo') == "jogo")
		{
			$this->load->model('jogo');
			$dadosfilme = $this->jogo->getPrevia($this->input->get('id'));
			$array = array('title'=>"Prévia", 'id'=>$this->input->get('id'), 'jogo'=>$dadosfilme, 'tipo'=>'Jogo');
			$this->load->view('header',$array);
			$this->load->view('previa',$array);
		}
	}

	public function aprovarsugestao()
	{
		if($this->input->get('tipo') == "filme")
		{
			$this->load->model('filme');
			$this->filme->aprovar($this->input->get('id'),$this->input->get('user'));
		}
		else if($this->input->get('tipo') == "jogo")
		{
			$this->load->model('jogo');
			$this->jogo->aprovar($this->input->get('id'),$this->input->get('user'));
		}
	}

	public function excluirsugestao()
	{
		if($this->input->get('tipo') == "filme")
		{
			$this->load->model('filme');
			$this->filme->excluirsugestao($this->input->get('id'),$this->input->get('user'));
		}
		else if($this->input->get('tipo') == "jogo")
		{
			$this->load->model('jogo');
			$this->jogo->excluirsugestao($this->input->get('id'),$this->input->get('user'));
		}
	}









}
