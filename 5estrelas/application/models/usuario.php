<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class usuario extends CI_Model{

	public $user;
	public $senha;

	public function __construct()
	{
        parent::__construct();
	}
	public function construtor($user,$senha)
	{
		$this->user = $user;
		$this->senha = md5($senha);
	}
	public function cadastrar()
	{
		$select = $this->db->query("SELECT * FROM usuarios WHERE user = '$this->user'");
      	foreach($select->result_array() as $array)
      	{
      		$veruser = $array['user'];
        }


//Verificação de irregularidades nos dados

      if(empty($this->senha))
        {
          echo"<script> alert('O campo senha deve ser preenchido'); </script>";
        }
      else if(empty($this->user))
        {
         echo"<script> alert('O campo usuário deve ser preenchido'); </script>";
        }
      else //Verificação de dados já existentes no BD
          {
          	if(isset($veruser))
          	{
                echo"<script>
                alert('Esse login já existe');
                </script>";                
                die();
              }
            else // Gravação dos Dados
              {
                $query = $this->db->query("INSERT INTO usuarios (user,senha) VALUES ('$this->user','$this->senha')"); 
                  echo "<script>
                  alert('Usuário cadastrado com sucesso!');
                  window.location.href='".base_url()."principal/paginalogin';        
                  </script>";
              }
          }


	}

 	public function logar()
 	{

 		$query = $this->db->query("SELECT senha from usuarios where user = '$this->user'");
 		if($query->num_rows() == 0)
 		{
 			echo "<script> alert('Esse login não existe'); 
      window.location.href='".base_url()."';
      </script>";
 		}
 		else
 		{
 			foreach ($query->result_array() as $row)
			{
       			 $senha = $row['senha'];
			}
			if($this->senha == $senha)
			{
        $this->db->query("insert into acessos (usuario) values ('".$this->user."')");
				echo "<script> alert('Logado');
				window.location.href='".base_url()."';
				</script>";
				$_SESSION['user'] = $this->user;
				$_SESSION['senha'] = $this->senha;
			}
			else
			{
				echo "<script> alert('Senha errada'); 
        window.location.href='".base_url()."';
        </script>";

			}


		}

 	}

  public function sair()
  {
    $_SESSION['user'] = 'convidado';
    $_SESSION['senha'] = 'senha'; 
    echo "<script> alert('Sessão finalizada'); 
    window.location.href='".base_url()."';
    </script>";
  }

  public function verificaAvaliacao($user,$tipo,$codigo)
  {
    if($tipo == 'filmes')
    {
      $resultadoverificacao = $this->db->query('SELECT * from avaliacoes_filmes where usuario = "'.$user.'" and codigo_avaliado = '.$codigo);
    }
    else if($tipo == 'jogos')
    {
      $resultadoverificacao = $this->db->query('SELECT * from avaliacoes_jogos where usuario = "'.$user.'" and codigo_avaliado = '.$codigo);
    }

    if($resultadoverificacao->num_rows() > 0)
    {
      return 1;
    }
    else
    {
      return 0;
    }
  }

  public function avaliar($user,$tipo,$id,$avaliacao,$nota)
  {
    if($tipo == "filme")
    {
      $query = $this->db->query("insert into avaliacoes_filmes (usuario, codigo_avaliado, avaliacao, nota) values ('".$user."',".$id.",'".$avaliacao."',".$nota.")");
      echo "<script> alert('Avaliação inserida com sucesso!');
      window.location.href='".base_url()."principal/exibirfilme/?id=".$id."';
      </script>";
    }
    else if($tipo == "jogo")
    {
      $query = $this->db->query("insert into avaliacoes_jogos (usuario, codigo_avaliado, avaliacao, nota) values ('".$user."',".$id.",'".$avaliacao."',".$nota.")");
      echo "<script> alert('Avaliação inserida com sucesso!');
      window.location.href='".base_url()."principal/exibirjogo/?id=".$id."';
      </script>";
    }

  }

  public function excluiravaliacao($user,$tipo,$id)
  {
    if($tipo == "filme")
    {
      $query = $this->db->query("delete from avaliacoes_filmes where usuario = '".$user."' and codigo_avaliado = ".$id);
      $query2 = $this->db->query("delete from denuncias where usuario = '".$user."' and codigo_avaliado = ".$id." and tipo='filme'");
      echo "<script> alert('Avaliação excluída com sucesso!');
      window.location.href='".base_url()."principal/exibirfilme/?id=".$id."';
      </script>";
    }
    else if($tipo == "jogo")
    {
      $query = $this->db->query("delete from avaliacoes_jogos where usuario = '".$user."' and codigo_avaliado = ".$id);
      $query2 = $this->db->query("delete from denuncias where usuario = '".$user."' and codigo_avaliado = ".$id." and tipo='jogo'");
      echo "<script> alert('Avaliação excluída com sucesso!');
      window.location.href='".base_url()."principal/exibirjogo/?id=".$id."';
      </script>";
    }
  }

  public function getAvaliacoes()
  {
    $query = $this->db->query("SELECT filmes.codigo as 'codigofilme', filmes.titulo as 'titulo', avaliacoes_filmes.usuario as 'usuario', avaliacoes_filmes.avaliacao as 'avaliacao', avaliacoes_filmes.nota as 'nota', avaliacoes_filmes.tipo as 'tipo' from filmes, avaliacoes_filmes where avaliacoes_filmes.usuario = '".$_SESSION['user']."' and filmes.codigo = avaliacoes_filmes.codigo_avaliado");
    $query2 = $this->db->query("SELECT jogos.codigo as 'codigojogo', jogos.titulo as 'titulo', avaliacoes_jogos.usuario as 'usuario', avaliacoes_jogos.avaliacao as 'avaliacao', avaliacoes_jogos.nota as 'nota', avaliacoes_jogos.tipo as 'tipo' from jogos, avaliacoes_jogos where avaliacoes_jogos.usuario = '".$_SESSION['user']."' and jogos.codigo = avaliacoes_jogos.codigo_avaliado");

    $result = array_merge($query->result_array(),$query2->result_array());
    return $result;

  }

  public function getAvaliacao($id,$tipo)
  {
    $av = $this->db->query("SELECT avaliacao, nota from avaliacoes_".$tipo."s where usuario = '".$_SESSION['user']."' and codigo_avaliado = ".$id);
    return $av->result_array();
  }

  public function editarAvaliacao($id,$tipo,$avaliacao,$nota)
  {
    $att = $this->db->query("UPDATE avaliacoes_".$tipo."s set avaliacao = '".$avaliacao."', nota =".$nota." where usuario = '".$_SESSION['user']."' and codigo_avaliado = ".$id);
    echo "<script> alert('Avaliação atualizada!');
      window.location.href='".base_url()."principal/exibir".$tipo."/?id=".$id."';
      </script>";

  }

  public function getCont()
  {
    $qntusers = $this->db->query("SELECT count(*) as 'qnt' from usuarios");
    foreach($qntusers->result_array() as $q)
    {
      $qf = $q['qnt'];
    }
    return $qf;
  }

  public function getQntAv()
  {
    $qntav = $this->db->query("SELECT count(*) as 'qnt' from avaliacoes_jogos");
    $qntav2 = $this->db->query("SELECT count(*) as 'qnt' from avaliacoes_filmes");
    foreach($qntav->result_array() as $q)
    {
      $q1 = $q['qnt'];
    }
    foreach ($qntav2->result_array() as $q) 
    {
      $q2 = $q['qnt'];
    }
    $qf = $q1+$q2;
    return $qf;
  }

  public function getContDenuncias()
  {
    $qntdenuncias = $this->db->query("SELECT count(*) as 'qnt' from denuncias where usuario = '".$_SESSION['user']."'");
    foreach($qntdenuncias->result_array() as $q)
    {
      $qf = $q['qnt'];
    }
    return $qf;
  }

  public function getAcessos()
  {
    $qntacessos = $this->db->query("SELECT count(*) as 'qnt' from acessos where usuario = '".$_SESSION['user']."'");
    foreach($qntacessos->result_array() as $q)
    {
      $qf = $q['qnt'];
    }
    return $qf;
  }

  public function denunciar($id,$tipo,$user)
  {
    $titulo = $this->db->query("select titulo from ".$tipo."s where codigo = ".$id);
    $avaliacao = $this->db->query("select avaliacao from avaliacoes_".$tipo."s where codigo_avaliado = ".$id." and usuario = '".$user."'");

    foreach($titulo->result_array() as $q)
    {
      $titulo = $q['titulo'];
    }
    foreach($avaliacao->result_array() as $q)
    {
      $avaliacao = $q['avaliacao'];
    }

    $denunciar = $this->db->query("INSERT into denuncias (usuario, codigo_avaliado, nome_av, tipo, avaliacao) values ('".$user."',".$id.",'".$titulo."','".$tipo."','".$avaliacao."')");
    echo "<script> alert('Denúncia Enviada!');
      window.location.href='".base_url()."principal/exibir".$tipo."/?id=".$id."';
      </script>";
  }

  public function getDenuncias()
  {
    $denuncias = $this->db->query("SELECT distinct usuario, nome_av, tipo, avaliacao, codigo_avaliado as 'id' from denuncias");
    return $denuncias->result_array();
  }

  public function excluirDenuncia($id,$tipo,$user)
  {
     $query2 = $this->db->query("delete from denuncias where usuario = '".$user."' and codigo_avaliado = ".$id." and tipo='".$tipo."'");
     echo "<script> alert('Denúncia excluída!');
      window.location.href='".base_url()."principal/paginadenuncias';
      </script>";
  }
  public function listarusers()
  {
    $query = $this->db->query("select user from usuarios where user != 'admin'");
    return $query->result_array();
  }
  public function excluirusuario($user)
  {
      $query = $this->db->query("delete from acessos where usuario = '".$user."'");
      $query2 = $this->db->query("delete from avaliacoes_jogos where usuario = '".$user."'");
      $query3 = $this->db->query("delete from avaliacoes_filmes where usuario = '".$user."'");
      $query4 = $this->db->query("delete from usuarios where user = '".$user."'");

      echo "<script> alert('Usuário excluído!');
      window.location.href='".base_url()."principal/listarusers';
      </script>";
  }

  public function recomendacao($tipo)
  {

    $favoritos = "(SELECT codigo_$tipo as '".$tipo."' from favoritos_".$tipo."s where usuario = '".$_SESSION['user']."')";
    $avaliados = "(SELECT codigo_avaliado as 'codigo' from avaliacoes_".$tipo."s where usuario = '".$_SESSION['user']."')";
    $cat = $this->db->query("SELECT categoria, count(*) as 'contador' from categorias_$tipo where ".$tipo." in $favoritos group by categoria order by count(*) desc limit 2");

    if($cat->result_array() != null)
    {
      $x = 1;
      foreach($cat->result_array() as $c)
      {
        $categorias[$x] = $c['categoria'];
        $x++;
      }

      $selecionados_1 = $this->db->query("CREATE VIEW peso1 as SELECT * from ".$tipo."s where codigo in (SELECT ".$tipo." as 'codigo' from categorias_".$tipo." where categoria = '".$categorias[1]."') and codigo not in $avaliados and codigo not in $favoritos order by RAND() limit 3");

      $q1 = $this->db->query("SELECT * from peso1");

      if($x == 3)
      {
        $selecionados_2 = $this->db->query("CREATE VIEW peso2 as SELECT * from ".$tipo."s where codigo in (SELECT ".$tipo." from categorias_".$tipo." where categoria = '".$categorias[2]."') and codigo not in $avaliados and codigo not in $favoritos and codigo NOT IN (select codigo from peso1) order by RAND() limit 2");

         $q2 = $this->db->query("SELECT * from peso2");

         $drop = $this->db->query("DROP view peso1");
         $drop2 = $this->db->query("DROP view peso2");

         $geral = array_merge($q1->result_array(),$q2->result_array());
      }
      else
      {

        $drop = $this->db->query("DROP view peso1");

        $geral = $q1->result_array();
      }

      return $geral;
    }

  }

  public function minhassugestoes()
  {
    $sugestoesf = $this->db->query("SELECT * FROM sugestoes_filmes where user = '".$_SESSION['user']."'");
    $sugestoesg = $this->db->query("SELECT * FROM sugestoes_jogos where user = '".$_SESSION['user']."'");

    $sugest = array_merge($sugestoesf->result_array(),$sugestoesg->result_array());
    return $sugest;
  }

  public function getSugestoes()
  {
    $sugestoesf = $this->db->query("SELECT titulo,user,tipo,codigo FROM sugestoes_filmes where estado = 'pendente'");
    $sugestoesg = $this->db->query("SELECT titulo,user,tipo,codigo FROM sugestoes_jogos where estado = 'pendente'");

    $sugest = array_merge($sugestoesf->result_array(),$sugestoesg->result_array());
    return $sugest;
  }


  

}
?>