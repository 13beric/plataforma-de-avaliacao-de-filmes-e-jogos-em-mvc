<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class jogo extends CI_Model
{

	public $titulo;
	public $lancamento;
	public $genero;
	public $plataformas;
	public $estudio;
	public $video;
	public $cartaz;
	public $faixa;


	public function __construct()
	{
        parent::__construct();
	}

	public function construtor($titulo, $estudio, $lancamento, $plataformas, $genero, $faixa, $video, $cartaz)
	{
		$this->titulo = $titulo;
		$this->lancamento = $lancamento;
		$this->estudio = $estudio;
		$this->plataformas = $plataformas;
		$this->genero = $genero;
		$this->video = $video;
		$this->cartaz = $cartaz;
		$this->faixa = $faixa;
	}

	public function cadastrar()
	{
		$error = array();

		if (!empty($this->cartaz["name"])) 
        {
    
    
          	$largura = 1500;
    
         	$altura = 1800;
  
          	$tamanho = 10000000;


		 	$largura = 1500;
    
          	$altura = 1800;
  
          	$tamanho = 10000000;


          	if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $this->cartaz["type"]))
          	{
            	$error[1] = "Isso não é uma imagem.";
          	} 
  
          	$dimensoes = getimagesize($this->cartaz["tmp_name"]);
  
          	if($dimensoes[0] > $largura) 
          	{
            	$error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
          	} 
 
          	if($dimensoes[1] > $altura) 
          	{
            	$error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
          	}
    
          	if($this->cartaz["size"] > $tamanho) 
          	{
            	$error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
          	}
        }


        if(count($error) == 0)
        {
          	preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $this->cartaz["name"], $ext);
 
          	$nome_imagem = md5(uniqid(time())) . "." . $ext[1];

          	$caminho_imagem = "cartazes/".$nome_imagem;
 
          	move_uploaded_file($this->cartaz["tmp_name"], $caminho_imagem);

          	$inserirdados = $this->db->query("INSERT INTO jogos (titulo, lancamento, genero, estudio, plataformas, faixa, video, cartaz) VALUES ('$this->titulo', '$this->lancamento','$this->genero','$this->estudio','$this->plataformas','$this->faixa','$this->video','$nome_imagem')");

             $idadd = $this->db->query("select codigo from jogos where titulo = '".$this->titulo."' and cartaz = '".$nome_imagem."'");
            foreach($idadd->result_array() as $q)
            {
              $idadd = $q['codigo'];
            }


            $categorias = explode(" / ", $this->genero);
            foreach($categorias as $q)
            {
              $inserircat = $this->db->query("INSERT into categorias_jogo (jogo, categoria) values (".$idadd.",'".$q."')");
            }

          	if ($inserirdados)
          	{
        	  	echo "<script> alert('Cadastro realizado com sucesso!');
        	  	window.location.href='".base_url()."principal/adicionar' </script>";
      	  	} 
    	}
  
    	if (count($error) != 0) 
    	{
      		foreach ($error as $erro) 
      		{
        		echo $erro . "<br />";
      		}
		}
	}

	public function listarjogos()
	{
		$buscajogos = $this->db->query("SELECT * FROM jogos order by titulo");
		return $buscajogos->result_array();
	}

  public function getDadosJogo($id)
  {
    $buscajogo = $this->db->query("SELECT * FROM jogos where codigo =".$id);
    return $buscajogo->result_array();
  }

  public function excluir($id)
  {
    $excluirjogo = $this->db->query("DELETE from jogos where codigo=".$id);
    echo "<script> alert('Jogo excluído!');
    window.location.href='".base_url()."principal/listarjogos' </script>";
  }
  public function busca($pesquisa)
  {
    $procura = $this->db->query("SELECT * from jogos where titulo like '%".$pesquisa."%'");
    return $procura->result_array();
  }

  public function getAvaliacoes($id)
  {
    $getav = $this->db->query("SELECT * from avaliacoes_jogos where codigo_avaliado =".$id);
    return $getav->result_array();
  }

  public function getNota($id)
  {
    $getnota = $this->db->query("SELECT avg(nota) as 'nota' from avaliacoes_jogos where codigo_avaliado = ".$id);
    return $getnota->result_array();
  }

 public function navegar($order,$categoria)
  {
    if($order == 'alpha')
    {
      if($categoria == 'não')
      {
        $getnav = $this->db->query("SELECT * from jogos order by titulo");
        return $getnav->result_array();
      }
      else
      {
        $getnav = $this->db->query("SELECT * from jogos where codigo in (select jogo from categorias_jogo where categoria = '".$categoria."') order by titulo");
        return $getnav->result_array();
      }
    }
    else if($order == 'nota')
    {
      if($categoria == 'não')
      {
        $getnav = $this->db->query("SELECT jogos.titulo, jogos.cartaz, jogos.codigo, jogos.tipo from jogos, avaliacoes_jogos where jogos.codigo = avaliacoes_jogos.codigo_avaliado group by avaliacoes_jogos.codigo_avaliado order by avg(avaliacoes_jogos.nota) desc");
        $getnav2 = $this->db->query("SELECT jogos.titulo, jogos.cartaz, jogos.codigo, jogos.tipo from jogos where codigo not in (select codigo_avaliado from avaliacoes_jogos)");
        $getnavfinal = array_merge($getnav->result_array(),$getnav2->result_array());
        return $getnavfinal;
      }
      else
      {
        $getnav = $this->db->query("SELECT jogos.titulo, jogos.cartaz, jogos.codigo, jogos.tipo from jogos, avaliacoes_jogos where jogos.codigo = avaliacoes_jogos.codigo_avaliado and jogos.codigo in (select jogo from categorias_jogo where categoria = '".$categoria."') group by avaliacoes_jogos.codigo_avaliado order by avg(avaliacoes_jogos.nota) desc");
        $getnav2 = $this->db->query("SELECT jogos.titulo, jogos.cartaz, jogos.codigo, jogos.tipo from jogos where codigo not in (select codigo_avaliado from avaliacoes_jogos) and codigo in (select jogo from categorias_jogo where categoria = '".$categoria."')");
        $getnavfinal = array_merge($getnav->result_array(),$getnav2->result_array());
        return $getnavfinal;
      }
    }
  }

    public function getCont()
  {
    $qntjogos = $this->db->query("SELECT count(*) as 'qnt' from jogos");
    foreach($qntjogos->result_array() as $q)
    {
      $cont = $q['qnt'];
    }
    return $cont;
  }

  public function getContUser()
  {
    $qntjogos = $this->db->query("SELECT count(*) as 'qnt' from avaliacoes_jogos where usuario = '".$_SESSION['user']."'");
    foreach($qntjogos->result_array() as $q)
    {
      $cont = $q['qnt'];
    }
    return $cont;
  }

  public function getCategorias()
  {
    $categorias = $this->db->query("SELECT distinct categoria from categorias_jogo");
    return $categorias->result_array();
  }
  public function verificarfavorito($id)
  {
    $query = $this->db->query("select * from favoritos_jogos where usuario = '".$_SESSION['user']."' and codigo_jogo = ".$id);
    $resultados = $query->num_rows();
    if($resultados > 0)
    {
      return 'sim';
    }
    else
    {
      return 'não';
    }
  }
  public function favoritar($id)
  {
    $favoritado = $this->verificarfavorito($id);
    if($favoritado == 'sim')
    {
      $query = $this->db->query("delete from favoritos_jogos where usuario = '".$_SESSION['user']."' and codigo_jogo = ".$id);
      echo "<script>
      alert('Jogo desfavoritado com sucesso!');
      window.location.href='".base_url()."principal/exibirjogo/?id=".$id."';        
      </script>";
    }
    else
    {
      $query = $this->db->query("insert into favoritos_jogos (usuario, codigo_jogo) values ('".$_SESSION['user']."',".$id.")");
      echo "<script>
      alert('Jogo favoritado com sucesso!');
      window.location.href='".base_url()."principal/exibirjogo/?id=".$id."';        
      </script>";
    }

  }
  public function parecidos($id)
  {
    $jogos = $this->db->query("SELECT titulo, cartaz, codigo from jogos where codigo in (SELECT distinct jogo as 'codigo' from categorias_jogo where jogo != ".$id." and categoria in (SELECT categoria from categorias_jogo where jogo = ".$id." )) limit 4");
    $tabela = $jogos->result_array();
    shuffle($tabela);
    return $tabela;
  }

  public function enviarsugestao()
  {
    $error = array();

    if (!empty($this->cartaz["name"])) 
        {
    
    
            $largura = 1500;
    
          $altura = 1800;
  
            $tamanho = 10000000;


      $largura = 1500;
    
            $altura = 1800;
  
            $tamanho = 10000000;


            if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $this->cartaz["type"]))
            {
              $error[1] = "Isso não é uma imagem.";
            } 
  
            $dimensoes = getimagesize($this->cartaz["tmp_name"]);
  
            if($dimensoes[0] > $largura) 
            {
              $error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
            } 
 
            if($dimensoes[1] > $altura) 
            {
              $error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
            }
    
            if($this->cartaz["size"] > $tamanho) 
            {
              $error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
            }
        }


        if(count($error) == 0)
        {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $this->cartaz["name"], $ext);
 
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

            $caminho_imagem = "cartazes/".$nome_imagem;
 
            move_uploaded_file($this->cartaz["tmp_name"], $caminho_imagem);

            $inserirdados = $this->db->query("INSERT INTO sugestoes_jogos (titulo, lancamento, genero, estudio, plataformas, faixa, video, cartaz, user) VALUES ('$this->titulo', '$this->lancamento','$this->genero','$this->estudio','$this->plataformas','$this->faixa','$this->video','$nome_imagem','".$_SESSION['user']."')");

            if ($inserirdados)
            {
              echo "<script> alert('Sugestão enviada com sucesso!');
              window.location.href='".base_url()."principal/sugerir' </script>";
            } 
      }
  
      if (count($error) != 0) 
      {
          foreach ($error as $erro) 
          {
            echo $erro . "<br />";
          }
    }
  }

public function getPrevia($codigo)
{
  $previa = $this->db->query("SELECT * from sugestoes_jogos where codigo = ".$codigo);
  return $previa->result_array();
}

public function aprovar($id,$user)
{
  $aprovar = $this->db->query("INSERT into jogos (titulo, lancamento, genero, estudio, plataformas, faixa, video, cartaz) select titulo, lancamento, genero, estudio, plataformas, faixa, video, cartaz from sugestoes_jogos where codigo = ".$id." and user = '".$user."'");
  $update = $this->db->query("update sugestoes_jogos set estado = 'aceita' where codigo = ".$id." and user = '".$user."'");

  echo "<script> alert('Sugestão aprovada!');
  window.location.href='".base_url()."principal/sugestoes' </script>";
}

public function excluirsugestao($id,$user)
{
  if($_SESSION['user'] == 'admin')
  {
    $excluir = $this->db->query("update sugestoes_jogos set estado = 'negada' where codigo = ".$id." and user = '".$user."'");
    echo "<script> alert('Sugestão negada!');
    window.location.href='".base_url()."principal/sugestoes' </script>";
  }
  else
  {
    $excluir = $this->db->query("delete from sugestoes_jogos where codigo = ".$id." and user = '".$user."'");
    echo "<script> alert('Sugestão excluída!');
    window.location.href='".base_url()."principal/minhassugestoes' </script>";
  }
}

	

}

?>