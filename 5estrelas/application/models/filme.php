<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class filme extends CI_Model
{

	public $titulo;
	public $lancamento;
	public $genero;
	public $elenco;
	public $direcao;
	public $video;
	public $cartaz;


	public function __construct()
	{
        parent::__construct();
	}

	public function construtor($titulo, $lancamento, $genero, $elenco, $direcao, $video, $cartaz, $faixa, $sinopse)
	{
		$this->titulo = $titulo;
		$this->lancamento = $lancamento;
		$this->genero = $genero;
		$this->elenco = $elenco;
		$this->direcao = $direcao;
		$this->video = $video;
		$this->cartaz = $cartaz;
    $this->faixa = $faixa;
    $this->sinopse = $sinopse;
	}

	public function Cadastrar()
	{
		$error = array();

		if (!empty($this->cartaz["name"])) 
        {
    
    
          	$largura = 1500;
    
         	$altura = 1800;
  
          	$tamanho = 10000000;


		 	$largura = 1500;
    
          	$altura = 1800;
  
          	$tamanho = 10000000;


          	if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $this->cartaz["type"]))
          	{
            	$error[1] = "Isso não é uma imagem.";
          	} 
  
          	$dimensoes = getimagesize($this->cartaz["tmp_name"]);
  
          	if($dimensoes[0] > $largura) 
          	{
            	$error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
          	} 
 
          	if($dimensoes[1] > $altura) 
          	{
            	$error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
          	}
    
          	if($this->cartaz["size"] > $tamanho) 
          	{
            	$error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
          	}
        }


        if(count($error) == 0)
        {
          	preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $this->cartaz["name"], $ext);
 
          	$nome_imagem = md5(uniqid(time())) . "." . $ext[1];

          	$caminho_imagem = "cartazes/".$nome_imagem;
 
          	move_uploaded_file($this->cartaz["tmp_name"], $caminho_imagem);

          	$inserirdados = $this->db->query("INSERT INTO filmes (titulo, lancamento, genero, elenco, direcao, video, cartaz, faixa, sinopse) VALUES ('$this->titulo', '$this->lancamento','$this->genero','$this->elenco','$this->direcao','$this->video','$nome_imagem', '$this->faixa', '$this->sinopse')");
            $idadd = $this->db->query("select codigo from filmes where titulo = '".$this->titulo."' and cartaz = '".$nome_imagem."'");
            foreach($idadd->result_array() as $q)
            {
              $idadd = $q['codigo'];
            }


            $categorias = explode(" / ", $this->genero);
            foreach($categorias as $q)
            {
              $inserircat = $this->db->query("INSERT into categorias_filme (filme, categoria) values (".$idadd.",'".$q."')");
            }

          	if ($inserirdados)
          	{
        	  	echo "<script> alert('Cadastro realizado com sucesso!');
        	  	window.location.href='".base_url()."principal/adicionar' </script>";
      	  	} 
    	}
  
    	if (count($error) != 0) 
    	{
      		foreach ($error as $erro) 
      		{
        		echo $erro . "<br />";
      		}
		}
	}

	public function listarfilmes()
	{
		$buscafilmes = $this->db->query("SELECT * FROM filmes order by titulo");
		return $buscafilmes->result_array();
	}

  public function getDadosFilme($id)
  {
    $buscafilme = $this->db->query("SELECT * FROM filmes where codigo =".$id);
    return $buscafilme->result_array();
  }
  public function excluir($id)
  {
    $excluirfilme = $this->db->query("DELETE from filmes where codigo=".$id);
    echo "<script> alert('Filme excluído!');
    window.location.href='".base_url()."principal/listarfilmes' </script>";
  }
  public function busca($pesquisa)
  {
    $procura = $this->db->query("SELECT * from filmes where titulo like '%".$pesquisa."%'");
    return $procura->result_array();
  }

  public function getAvaliacoes($id)
  {
    $getav = $this->db->query("SELECT * from avaliacoes_filmes where codigo_avaliado =".$id);
    return $getav->result_array();
  }

  public function getNota($id)
  {
    $getnota = $this->db->query("SELECT avg(nota) as 'nota' from avaliacoes_filmes where codigo_avaliado = ".$id);
    return $getnota->result_array();
  }

  public function navegar($order,$categoria)
  {
    if($order == 'alpha')
    {
      if($categoria == 'não')
      {
        $getnav = $this->db->query("SELECT * from filmes order by titulo");
        return $getnav->result_array();
      }
      else
      {
        $getnav = $this->db->query("SELECT * from filmes where codigo in (select filme from categorias_filme where categoria = '".$categoria."') order by titulo");
        return $getnav->result_array();
      }
    }
    else if($order == 'nota')
    {
      if($categoria == 'não')
      {
        $getnav = $this->db->query("SELECT filmes.titulo, filmes.cartaz, filmes.codigo, filmes.tipo from filmes, avaliacoes_filmes where filmes.codigo = avaliacoes_filmes.codigo_avaliado group by avaliacoes_filmes.codigo_avaliado order by avg(avaliacoes_filmes.nota) desc");
        $getnav2 = $this->db->query("SELECT filmes.titulo, filmes.cartaz, filmes.codigo, filmes.tipo from filmes where codigo not in (select codigo_avaliado from avaliacoes_filmes)");
        $getnavfinal = array_merge($getnav->result_array(),$getnav2->result_array());
        return $getnavfinal;
      }
      else
      {
        $getnav = $this->db->query("SELECT filmes.titulo, filmes.cartaz, filmes.codigo, filmes.tipo from filmes, avaliacoes_filmes where filmes.codigo = avaliacoes_filmes.codigo_avaliado and filmes.codigo in (select filme from categorias_filme where categoria = '".$categoria."') group by avaliacoes_filmes.codigo_avaliado order by avg(avaliacoes_filmes.nota) desc");
        $getnav2 = $this->db->query("SELECT filmes.titulo, filmes.cartaz, filmes.codigo, filmes.tipo from filmes where codigo not in (select codigo_avaliado from avaliacoes_filmes) and codigo in (select filme from categorias_filme where categoria = '".$categoria."')");
        $getnavfinal = array_merge($getnav->result_array(),$getnav2->result_array());
        return $getnavfinal;
      }
    }
  }

  public function getCont()
  {
    $qntfilmes = $this->db->query("SELECT count(*) as 'qnt' from filmes");
    foreach($qntfilmes->result_array() as $q)
    {
      $cont = $q['qnt'];
    }
    return $cont;
  }

  public function getContUser()
  {
    $qntfilmes = $this->db->query("SELECT count(*) as 'qnt' from avaliacoes_filmes where usuario = '".$_SESSION['user']."'");
    foreach($qntfilmes->result_array() as $q)
    {
      $cont = $q['qnt'];
    }
    return $cont;
  }

  public function getCategorias()
  {
    $categorias = $this->db->query("SELECT distinct categoria from categorias_filme");
    return $categorias->result_array();
  }

  public function verificarfavorito($id)
  {
    $query = $this->db->query("select * from favoritos_filmes where usuario = '".$_SESSION['user']."' and codigo_filme = ".$id);
    $resultados = $query->num_rows();
    if($resultados > 0)
    {
      return 'sim';
    }
    else
    {
      return 'não';
    }
  }

  public function favoritar($id)
  {
    $favoritado = $this->verificarfavorito($id);
    if($favoritado == 'sim')
    {
      $query = $this->db->query("delete from favoritos_filmes where usuario = '".$_SESSION['user']."' and codigo_filme = ".$id);
      echo "<script>
      alert('Filme desfavoritado com sucesso!');
      window.location.href='".base_url()."principal/exibirfilme/?id=".$id."';        
      </script>";
    }
    else
    {
      $query = $this->db->query("insert into favoritos_filmes (usuario, codigo_filme) values ('".$_SESSION['user']."',".$id.")");
      echo "<script>
      alert('Filme favoritado com sucesso!');
      window.location.href='".base_url()."principal/exibirfilme/?id=".$id."';        
      </script>";
    }

  }

  public function parecidos($id)
  {
    $filmes = $this->db->query("SELECT titulo, cartaz, codigo from filmes where codigo in (SELECT distinct filme as 'codigo' from categorias_filme where filme != ".$id." and categoria in (SELECT categoria from categorias_filme where filme = ".$id." )) limit 4");
    $tabela = $filmes->result_array();
    shuffle($tabela);
    return $tabela;
  }

  public function enviarsugestao()
  {
    {
    $error = array();

    if (!empty($this->cartaz["name"])) 
        {
    
    
            $largura = 1500;
    
          $altura = 1800;
  
            $tamanho = 10000000;


      $largura = 1500;
    
            $altura = 1800;
  
            $tamanho = 10000000;


            if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $this->cartaz["type"]))
            {
              $error[1] = "Isso não é uma imagem.";
            } 
  
            $dimensoes = getimagesize($this->cartaz["tmp_name"]);
  
            if($dimensoes[0] > $largura) 
            {
              $error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
            } 
 
            if($dimensoes[1] > $altura) 
            {
              $error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
            }
    
            if($this->cartaz["size"] > $tamanho) 
            {
              $error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
            }
        }


        if(count($error) == 0)
        {
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $this->cartaz["name"], $ext);
 
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];

            $caminho_imagem = "cartazes/".$nome_imagem;
 
            move_uploaded_file($this->cartaz["tmp_name"], $caminho_imagem);

            $inserirdados = $this->db->query("INSERT INTO sugestoes_filmes (titulo, lancamento, genero, elenco, direcao, video, cartaz, faixa, sinopse,user) VALUES ('$this->titulo', '$this->lancamento','$this->genero','$this->elenco','$this->direcao','$this->video','$nome_imagem', '$this->faixa', '$this->sinopse', '".$_SESSION['user']."')");
            $idadd = $this->db->query("select codigo from filmes where titulo = '".$this->titulo."' and cartaz = '".$nome_imagem."'");

            if ($inserirdados)
            {
              echo "<script> alert('Sugestâo enviada com sucesso!');
              window.location.href='".base_url()."principal/sugerir' </script>";
            } 
      }
  
      if (count($error) != 0) 
      {
          foreach ($error as $erro) 
          {
            echo $erro . "<br />";
          }
    }
  }
}

public function getPrevia($codigo)
{
  $previa = $this->db->query("SELECT * from sugestoes_filmes where codigo = ".$codigo);
  return $previa->result_array();
}

public function aprovar($id,$user)
{
  $aprovar = $this->db->query("INSERT into filmes (titulo, lancamento, genero, elenco, direcao, video, cartaz, faixa, sinopse) select titulo, lancamento, genero, elenco, direcao, video, cartaz, faixa, sinopse from sugestoes_filmes where codigo = ".$id." and user = '".$user."'");
  $update = $this->db->query("update sugestoes_filmes set estado = 'aceita' where codigo = ".$id." and user = '".$user."'");

  echo "<script> alert('Sugestão aprovada!');
  window.location.href='".base_url()."principal/sugestoes' </script>";

}

public function excluirsugestao($id,$user)
{
  if($_SESSION['user'] == 'admin')
  {
    $excluir = $this->db->query("update sugestoes_filmes set estado = 'negada' where codigo = ".$id." and user = '".$user."'");
    echo "<script> alert('Sugestão negada!');
    window.location.href='".base_url()."principal/sugestoes' </script>";
  }
  else
  {
    $excluir = $this->db->query("delete from sugestoes_filmes where codigo = ".$id." and user = '".$user."'");
    echo "<script> alert('Sugestão excluída!');
    window.location.href='".base_url()."principal/minhassugestoes' </script>";
  }
}

}

?>