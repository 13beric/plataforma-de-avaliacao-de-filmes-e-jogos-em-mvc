-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 01-Dez-2017 às 21:50
-- Versão do servidor: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projeto`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `acessos`
--

DROP TABLE IF EXISTS `acessos`;
CREATE TABLE IF NOT EXISTS `acessos` (
  `usuario` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `acessos`
--

INSERT INTO `acessos` (`usuario`) VALUES
('beric'),
('beric'),
('admin'),
('usercomum'),
('admin'),
('beric'),
('admin'),
('beric'),
('usercomum'),
('admin'),
('beric'),
('admin'),
('admin'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('admin'),
('admin'),
('beric'),
('admin'),
('beric'),
('fulano'),
('admin'),
('beric'),
('admin'),
('beric'),
('admin'),
('joao'),
('admin'),
('beric'),
('admin'),
('admin'),
('beric'),
('admin'),
('admin'),
('admin'),
('beric'),
('admin'),
('muhammad'),
('ViníciusBraga'),
('ViníciusBraga'),
('beric'),
('admin'),
('admin'),
('admin'),
('admin'),
('beric'),
('eric'),
('admin'),
('admin'),
('beric'),
('admin'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('admin'),
('beric'),
('eric123'),
('beric'),
('eric123'),
('admin'),
('admin'),
('admin'),
('beric'),
('eric123'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('beric'),
('beric'),
('beric'),
('eric123'),
('beric'),
('eric'),
('beric'),
('eric'),
('beric'),
('beric'),
('beric'),
('admin'),
('beric'),
('beric'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('testefav'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('beric'),
('beric'),
('admin'),
('beric'),
('admin'),
('beric'),
('beric'),
('leo_sasse'),
('admin'),
('leo_sasse'),
('admin'),
('beric'),
('admin'),
('admin'),
('admin');

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacoes_filmes`
--

DROP TABLE IF EXISTS `avaliacoes_filmes`;
CREATE TABLE IF NOT EXISTS `avaliacoes_filmes` (
  `usuario` varchar(100) NOT NULL,
  `codigo_avaliado` varchar(100) NOT NULL,
  `avaliacao` text NOT NULL,
  `nota` double NOT NULL,
  `tipo` varchar(10) NOT NULL DEFAULT 'filme',
  PRIMARY KEY (`usuario`,`codigo_avaliado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `avaliacoes_filmes`
--

INSERT INTO `avaliacoes_filmes` (`usuario`, `codigo_avaliado`, `avaliacao`, `nota`, `tipo`) VALUES
('beric', '13', 'Melhor filme já feito na história!', 5, 'filme'),
('beric', '2', 'Então, Carros 3 é um filme para fãs. Quem é fã vai amar, por causa de suas referências e também porque tem um tom de despedida em grande estilo. O roteiro é bem previsível, só que no final tem uma \"mini virada de roteiro\" que é construída a partir do segundo ato. Carros 3 tem recebido críticas não tão boas assim, mas elas não são feitas por fãs e sim por pessoas que não são simpatizantes. Eu como fã amei carros 3, muita nostalgia para quem viu desde pequeno essa franquia que considero a melhor da pixar. Esse filme mostra um empoderamento feminino de forma muito tranquila e que achei muito legal. Espero que tenha gostado dessa crítica e avalie e dê sua opinião.', 4, 'filme'),
('beric', '3', 'Sem dúvidas um clássico os efeitos especiais que impressionaram o público e agradaram a muitas fans da série, uma das melhores sequências do gênero da ficção cientifica e um dos melhores filmes que eu já vi simplesmente um filme cheio de cenas clássicas que quase ninguém poderia esquecer e tambem sem falar nas ótimas atuções dos atores e no roteiro um filme que eternizou personagens geniais simplesmente uma obra prima.', 5, 'filme'),
('beric', '6', 'O Hobbit: a Batalha dos Cinco Exércitos continua de forma bem atraente a saga dirigida por Peter Jackson. O filme não é totalmente fiel ao livro, mas esse foi um dos pontos que mais achei interessante, pois conseguiram adaptar de forma bem clara, dando a entender melhor a história e não alterando de forma drástica o seu final, conseguindo assim uma boa continuidade. Ação e bons efeitos não faltam, embora o segundo filme tenha sido mais convincente, com um tom mais sombrio e sangrento. Houveram algumas partes desnecessárias, deixando o filme mais longo do que realmente deveria ser, mas como é o capítulo final nós, fãs, adoramos cada parte. A trilha sonora também não deixou a desejar, deixando as cenas ainda melhores. A música The Last Goodbye, com sua letra comovente e cantada por Billy Boyd (Pippin, O Senhor dos Anéis) deu aos fãs um misto de alegria e tristeza e deixando-os cada vez mais apaixonados pelas aventuras do hobbit Bilbo Bolseiro. Com boas atuações,bom enredo e cenas de batalha emocionantes, valeu à pena esperar.', 4, 'filme'),
('beric', '7', 'É um filme bacana. Efeitos especiais (na maioria das vezes) são excepcionais. As atuações também foram boas e o roteiro mediano. Infelizmente a prolongação com muitas cenas sem nexo poderiam ser retiradas, como a dos pais de Sam na faculdade e as aparições de Megan Fox no começo do filme como um patrocínio para o filme, diante de seu corpo. A Vingança Dos Derrotados foi cansativo, e eu digo isso exageradamente, por isso procure ver o filme com muito gás de disposição, e em busca de muita diversão, pois vale a pena. Aliás, grandes atuações de Josh Duhamel e John Turturro.', 3, 'filme'),
('leo_sasse', '14', 'FILME MORALIZADOR!!!', 5, 'filme'),
('usercomum', '5', 'Muito bom ', 5, 'filme');

-- --------------------------------------------------------

--
-- Estrutura da tabela `avaliacoes_jogos`
--

DROP TABLE IF EXISTS `avaliacoes_jogos`;
CREATE TABLE IF NOT EXISTS `avaliacoes_jogos` (
  `usuario` varchar(100) NOT NULL,
  `codigo_avaliado` int(11) NOT NULL,
  `avaliacao` text NOT NULL,
  `nota` double NOT NULL,
  `tipo` varchar(10) NOT NULL DEFAULT 'jogo',
  PRIMARY KEY (`usuario`,`codigo_avaliado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `avaliacoes_jogos`
--

INSERT INTO `avaliacoes_jogos` (`usuario`, `codigo_avaliado`, `avaliacao`, `nota`, `tipo`) VALUES
('beric', 7, 'lol é melhor que isso', 0, 'jogo'),
('ViníciusBraga', 3, 'Jogo muito divertido. Mas não jogue-o :D', 0, 'jogo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias_filme`
--

DROP TABLE IF EXISTS `categorias_filme`;
CREATE TABLE IF NOT EXISTS `categorias_filme` (
  `filme` int(11) NOT NULL,
  `categoria` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categorias_filme`
--

INSERT INTO `categorias_filme` (`filme`, `categoria`) VALUES
(2, 'Aventura'),
(2, 'Animação'),
(2, 'Família'),
(3, 'Aventura'),
(3, 'Ficção Científica'),
(5, 'Ação'),
(5, 'Policial'),
(5, 'Suspense'),
(6, 'Ação'),
(6, 'Fantasia'),
(7, 'Ação'),
(7, 'Ficção científica'),
(8, 'Ação'),
(8, 'Fantasia'),
(8, 'Aventura'),
(8, 'Ficção científica'),
(9, 'Aventura'),
(9, 'Fantasia'),
(10, 'Ação'),
(11, 'Ficção Científica'),
(11, 'Ação'),
(11, 'Aventura'),
(12, 'Ação'),
(12, 'Ficção Científica'),
(13, 'Comédia'),
(13, 'Fantasia'),
(14, 'Fantasia'),
(14, 'Animação'),
(14, 'Comédia Dramática');

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias_jogo`
--

DROP TABLE IF EXISTS `categorias_jogo`;
CREATE TABLE IF NOT EXISTS `categorias_jogo` (
  `jogo` int(11) NOT NULL,
  `categoria` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categorias_jogo`
--

INSERT INTO `categorias_jogo` (`jogo`, `categoria`) VALUES
(3, 'MOBA'),
(4, 'FPS'),
(5, 'FPS'),
(6, 'Esporte'),
(6, 'Simulação'),
(7, 'MOBA'),
(8, 'TPS'),
(8, 'Construção'),
(8, 'Sandbox'),
(8, 'Battle Royal'),
(9, 'Esporte'),
(9, 'Simulação'),
(10, 'FPS'),
(11, 'Simulação'),
(12, 'Simulação'),
(13, 'RPG'),
(13, 'Sandbox'),
(13, 'Indie');

-- --------------------------------------------------------

--
-- Estrutura da tabela `denuncias`
--

DROP TABLE IF EXISTS `denuncias`;
CREATE TABLE IF NOT EXISTS `denuncias` (
  `usuario` varchar(50) NOT NULL,
  `codigo_avaliado` int(11) NOT NULL,
  `nome_av` varchar(50) NOT NULL,
  `tipo` varchar(10) NOT NULL,
  `avaliacao` varchar(1000) NOT NULL,
  `resolvido` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `denuncias`
--

INSERT INTO `denuncias` (`usuario`, `codigo_avaliado`, `nome_av`, `tipo`, `avaliacao`, `resolvido`) VALUES
('usercomum', 5, 'Batman: O Cavaleiro das Trevas', 'filme', 'Muito bom ', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `favoritos_filmes`
--

DROP TABLE IF EXISTS `favoritos_filmes`;
CREATE TABLE IF NOT EXISTS `favoritos_filmes` (
  `usuario` varchar(50) NOT NULL,
  `codigo_filme` int(11) NOT NULL,
  PRIMARY KEY (`usuario`,`codigo_filme`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `favoritos_filmes`
--

INSERT INTO `favoritos_filmes` (`usuario`, `codigo_filme`) VALUES
('beric', 7),
('beric', 10),
('beric', 13),
('beric', 14),
('leo_sasse', 14);

-- --------------------------------------------------------

--
-- Estrutura da tabela `favoritos_jogos`
--

DROP TABLE IF EXISTS `favoritos_jogos`;
CREATE TABLE IF NOT EXISTS `favoritos_jogos` (
  `usuario` varchar(50) NOT NULL,
  `codigo_jogo` int(11) NOT NULL,
  PRIMARY KEY (`usuario`,`codigo_jogo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `favoritos_jogos`
--

INSERT INTO `favoritos_jogos` (`usuario`, `codigo_jogo`) VALUES
('beric', 5),
('beric', 6),
('beric', 11),
('eric', 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `filmes`
--

DROP TABLE IF EXISTS `filmes`;
CREATE TABLE IF NOT EXISTS `filmes` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `lancamento` varchar(50) NOT NULL,
  `genero` varchar(150) NOT NULL,
  `elenco` varchar(500) NOT NULL,
  `direcao` varchar(50) NOT NULL,
  `video` varchar(200) NOT NULL,
  `cartaz` varchar(200) NOT NULL,
  `faixa` int(11) NOT NULL,
  `sinopse` text NOT NULL,
  `tipo` varchar(10) NOT NULL DEFAULT 'filme',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `filmes`
--

INSERT INTO `filmes` (`codigo`, `titulo`, `lancamento`, `genero`, `elenco`, `direcao`, `video`, `cartaz`, `faixa`, `sinopse`, `tipo`) VALUES
(2, 'Carros 3', '13/07/2017', 'Animação, Aventura, Família', ' Giovanna Ewbank, Fernanda Gentil, Owen Wilson', 'Brian Fee', 'BuvJZGLclAU', '989d589fe5c346b0c11129c592f63945.jpg', 0, 'Surpreendido por uma nova geração de corredores incrivelmente rápidos, o lendário Relâmpago McQueen é repentinamente afastado do esporte que ama. Para voltar com tudo às corridas, ele precisará da ajuda de uma determinada jovem treinadora de corridas, Cruz Ramirez. Com o seu plano para vencer, mais a inspiração do Fabuloso Doc Hudson e alguns acontecimentos inesperados, eles partem para a maior aventura de suas vidas. E o teste final do campeão será na maior prova da Copa Pistão!', 'filme'),
(3, 'Star Wars: O Império Contra-Ataca', '21/05/1980', 'Aventura / Ficção Científica', 'Mark Hamill  / Harrison Ford / Carrie Fisher / Billy Dee Williams / Anthony Daniels / David Prowse / Peter Mayhew / Kenny Baker / Frank Oz', 'Irvin Kershner', '7wal8pfpZ0o', '3c214b81387a2c5f67f9a93228e4fb56.jpg', 0, 'Yoda treina Luke Skywalker para ser um cavaleiro Jedi. Han Solo corteja a Princesa Leia enquanto Darth Vader retorna para combater as forças rebeldes que tentam salvar a galáxia.', 'filme'),
(5, 'Batman: O Cavaleiro das Trevas', '18/07/2008', 'Ação / Policial / Suspense', 'Christian Bale / Michael Caine / Heath Ledger / Gary Oldman  / Aaron Eckhart / Maggie Gyllenhaal / Morgan Freeman', 'Christopher Nolan', 'dkcB6SvkWrk', '2abd294550043afa0d32007cbc8e5f7f.jpg', 0, 'Com a ajuda de Jim Gordon e Harvey Den, Batman tem mantido a ordem na cidade de Gotham. Porém quando um jovem criminoso, que se chama Coringa, aparece e arma na cidade um verdadeiro caos, Batmam precisa encontrar uma maneira de detê-lo antes que mais vidas sejam perdidas.', 'filme'),
(6, 'O Hobbit: A Batalha dos Cinco Exércitos', '07/12/2014', 'Fantasia / Ação', 'Martin Freeman / Ian McKellen / Richard Armitage / Evangeline Lilly / Luke Evans / Lee Pace Benedict / Cumberbatch Ken Stott / James Nesbitt / Christopher Lee / Ian Holm / Hugo Weaving / Cate Blanchett / Orlando Bloom', 'Peter Jackson', 'hftfDwib1ds', 'd7a80fce376b08788996ea62e0b05a77.jpg', 14, 'O dragão Smaug lança sua fúria ardente contra a Cidade do Lago que fica próxima da montanha de Erebor. Bard consegue derrotá-lo, mas, rapidamente, sem a ameaça do dragão, inicia-se uma batalha pelo controle de Erebor e sua riqueza. Os anões, liderados por Thorin, adentram a montanha e estão dispostos a impedir a entrada de elfos, anões e orcs. Enquanto isso, Bilbo Bolseiro e Gandalf tentam impedir a guerra.', 'filme'),
(7, 'Transformers: A Vingança dos Derrotados', '24/06/2009', 'Ação / Ficção científica', 'Shia LaBeouf / Megan Fox / Josh Duhamel / Tyrese Gibson / Kevin Dunn / Julie White / John Turturro', 'Michael Bay', '_oz4imdUkJc', '59594b633f560c11d5cf46b653b3d35a.jpg', 10, 'Dois anos após ele e seus amigos Autobots salvarem a Terra dos Decepticons, Sam Witwicky encara uma nova batalha: a universidade. Ao mesmo tempo, Optimus Prime e os Autobots estão trabalhando com uma organização militar secreta e tentam construir uma casa para eles na Terra. Quando um antigo Decepticon conhecido como The Fallen aparece para se vingar, Sam e sua namorada Mikaela devem desvendar a história dos Transformers e achar um caminho para derrotar The Fallen.', 'filme'),
(8, 'Thor: Ragnarok', '26/10/2017', 'Ação / Fantasia / Aventura / Ficção científica', 'Chris Hemsworth / Tom Hiddleston / Cate Blanchett / Idris Elba / Tessa Thompson / Jeff Goldblum / Karl Urban / Mark Ruffalo / Anthony Hopkins', 'Taika Waititi', 'UvNnqWLruXA', 'a3dfd40d94f93ab502016b5143258e6a.jpg', 12, 'No filme da Marvel Studios, \"Thor: Ragnarok\", Thor é preso do outro lado do universo, sem o seu martelo poderoso e encontra-se numa corrida contra o tempo para voltar a Asgard e impedir Ragnarok - a destruição do seu mundo e o fim da civilização Asgardiana -, que se encontra nas mãos de uma nova e poderosa ameaça, a implacável Hela. Mas, primeiro precisa de sobreviver a uma luta mortal de gladiadores, que o coloca contra um ex-aliado e companheiro Vingador - Hulk.', 'filme'),
(9, 'Harry Potter e o Prisioneiro de Azkaban', '31/05/2004', 'Aventura / Fantasia', 'Daniel Radcliffe / Rupert Grint / Emma Watson / Michael Gambon / Gary Oldman', 'Alfonso Cuarón', 'SOb86UZnA8M', 'ea6b9aebed5e76ff9fabe8c5f05f812e.jpg', 0, 'O terceiro ano de Harry Potter em Hogwarts começa mal quando ele descobre que o assassino Sirius Black escapou da prisão de Azkaban e está empenhado em matá-lo. Enquanto o gato de Hermione atormenta o rato doente de Ron, um bando de dementadores são enviados para proteger a escola de Sirius Black. Um professor misterioso ajuda Harry a aprender a se defender.', 'filme'),
(10, 'Velozes & Furiosos 7', '02/04/2015', 'Ação', 'Vin Diesel / Paul Walker / Dwayne Johnson / Michelle Rodriguez / Tyrese Gibson / Ludacris / Jordana Brewster / Djimon Hounsou / Kurt Russell / Jason Statham', 'James Wan', 'Skpu5HaVkOc', '96bb6c1a57f18d481812c41a5a2206a0.jpg', 14, 'Após os acontecimentos em Londres, Dom, Brian, Letty e o resto da equipe têm a chance de voltar para os Estados Unidos e recomeçar suas vidas. Mas a tranquilidade do grupo é destruída quando Deckard Shaw, um assassino profissional, quer vingança pelo acidente que deixou seu irmão em coma. Agora, a equipe tem de unir forças para deter um vilão novo e ainda mais perigoso. Dessa vez, não se trata apenas de uma questão de velocidade: a corrida é pela sobrevivência.', 'filme'),
(11, 'O Exterminador do Futuro: Gênesis', '25/06/2015', 'Ficção Científica / Ação / Aventura', 'Arnold Schwarzenegger / Emilia Clarke / Jason Clarke / Jai Courtney / Dayo Okeniyi / Byung-hun Lee / J. K. Simmons / Matt Smith', 'Alan Taylor', 'yHLyOLE6YW4', 'ce9f0ae76e43dc7f66576ceb58502084.jpg', 12, 'John Connor, líder da resistência humana contra Skynet, envia Kyle Reese de volta a 1984 para proteger sua mãe, Sarah. Porém, Reese se surpreende ao encontrá-la com um exterminador que age como seu protetor, e descobre que o passado foi alterado.', 'filme'),
(12, 'Vingadores: Era de Ultron', '13/04/2015', 'Ação / Ficção Científica', 'Robert Downey, Jr. / Chris Hemsworth / Mark Ruffalo / Chris Evans / Scarlett Johansson / Jeremy Renner / Don Cheadle / Aaron Taylor-Johnson / Elizabeth Olsen / Paul Bettany / Cobie Smulders / Anthony Mackie / Hayley Atwell / Idris Elba / Stellan Skarsgård / James Spader / Samuel L. Jackson', 'Joss Whedon', 'Tg8hx9JKEZs', '50d2107ff33d0f8c9b3b0b50c955d3c8.jpg', 10, 'Ao tentar proteger o planeta de ameaças, Tony Stark constrói um sistema de inteligência artificial que cuidaria da paz mundial. O projeto acaba dando errado e gera o nascimento do Ultron. Com o destino da Terra em jogo, Capitão América, Homem de Ferro, Thor, Hulk, Viúva Negra e Gavião Arqueiro terão que se unir para mais uma vez salvar a raça humana da extinção.', 'filme'),
(13, 'Herbie: Meu Fusca Turbinado', '15/07/2005', 'Comédia / Fantasia', 'Lindsay Lohan / Justin Long / Michael Keaton / Matt Dillon / Breckin Meyer', 'Angela Robinson', 'dfdNbtiscbw', 'e8754f9132a1e907460b817a255acb7d.jpg', 0, 'Maggie Peyton quer se tornar um piloto da NASCAR, mas seu pai superprotetor, Ray Peyton, não quer. Ray, que foi um piloto, quer que Maggie arrume um emprego de locutora de esportes para TV porque é mais lucrativo e deixe a pista para seu irmão. Quando Ray leva Maggie a um ferro-velho para escolher um carro, ela não tem ideia de que o pequeno Volkswagen chamado Herbie que ela leva para casa vai mudar sua vida.', 'filme'),
(14, 'Toy Story 3', '12/06/2010', 'Fantasia / Animação / Comédia Dramática', 'Tom Hanks / Tim Allen / Joan Cusack / Ned Beatty / Don Rickles / Estelle Harris / Wallace Shawn  / John Ratzenberger / Blake Clark / Jodi Benson', 'Lee Unkrich', 'JcpWXaA2qeg', 'bb8945d3f0c0d9e8406e929767e69bb3.jpg', 0, 'Andy se prepara para ir para a faculdade e, inesperadamente, seus leais brinquedos acabando sendo enviados para uma creche. As crianças e seus dedinhos pegajosos não têm cuidado ao brincar, de modo que os brinquedos decidem unir forças e planejar uma grande fuga. Algumas carinhas novas, umas de plástico e outras de pelúcia, juntam-se a essa incrível aventura: Ken, o namorado de Barbie, um ouriço-ator chamado Espeto e um ursinho de pelúcia rosa e com cheirinho de morango chamado Lotso.', 'filme'),
(15, 'O Lobo de Wall Street', '24/01/2014', 'Biografia / Drama / Policial', 'Leonardo Dicaprio', 'Martin Scorsese', 'PoSCUsNQVtw', 'b6c9bef8c48b1476b739425dafdc9df9.jpg', 18, 'Durante seis meses, Jordan Belfort (Leonardo DiCaprio) trabalhou duro em uma corretora de Wall Street, seguindo os ensinamentos de seu mentor Mark Hanna (Matthew McConaughey). Quando finalmente consegue ser contratado como corretor da firma, acontece o Black Monday, que faz com que as bolsas de vários países caiam repentinamente. Sem emprego e bastante ambicioso, ele acaba trabalhando para uma empresa de fundo de quintal que lida com papéis de baixo valor, que não estão na bolsa de valores. É lá que Belfort tem a ideia de montar uma empresa focada neste tipo de negócio, cujas vendas são de valores mais baixos mas, em compensação, o retorno para o corretor é bem mais vantajoso. Ao lado de Donnie (Jonah Hill) e outros amigos dos velhos tempos, ele cria a Stratton Oakmont, uma empresa que faz com que todos enriqueçam rapidamente e, também, levem uma vida dedicada ao prazer.', 'filme'),
(16, 'Liga da Justiça', '15/11/2017', 'Ação / Aventura / Fantasia / Ficção científica', 'Ben Affleck / Henry Cavill / Amy Adams / Gal Gadot / Ezra Miller / Jason Momoa / Ray Fisher / Jeremy Irons / Diane Lane / Connie Nielsen / J.K. Simmons', 'Zack Snyder', '9UCgwO0ft0Q', '83d7321cc2b7e78c25758898c9558bb3.jpg', 12, 'Impulsionado pela restauração de sua fé na humanidade e inspirado pelo ato altruísta do Superman (Henry Cavill), Bruce Wayne (Ben Affleck) convoca sua nova aliada Diana Prince (Gal Gadot) para o combate contra um inimigo ainda maior, recém-despertado. Juntos, Batman e Mulher-Maravilha buscam e recrutam com agilidade um time de meta-humanos, mas mesmo com a formação da liga de heróis sem precedentes - Batman, Mulher-Maraviha, Aquaman (Jason Momoa), Cyborg (Ray Fisher) e Flash (Ezra Miller) -, poderá ser tarde demais para salvar o planeta de um catastrófico ataque.', 'filme');

-- --------------------------------------------------------

--
-- Estrutura da tabela `jogos`
--

DROP TABLE IF EXISTS `jogos`;
CREATE TABLE IF NOT EXISTS `jogos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `estudio` varchar(100) NOT NULL,
  `plataformas` varchar(150) NOT NULL,
  `genero` varchar(100) NOT NULL,
  `faixa` varchar(100) NOT NULL,
  `lancamento` varchar(100) NOT NULL,
  `video` varchar(200) NOT NULL,
  `cartaz` varchar(200) NOT NULL,
  `tipo` varchar(10) NOT NULL DEFAULT 'jogo',
  PRIMARY KEY (`codigo`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `jogos`
--

INSERT INTO `jogos` (`codigo`, `titulo`, `estudio`, `plataformas`, `genero`, `faixa`, `lancamento`, `video`, `cartaz`, `tipo`) VALUES
(3, 'League of Legends', 'Riot Games', 'PC', 'MOBA', 'Livre', '27/08/2009', 'RprbAMOPsH0', 'c27a3d1a3623c869f93979fa2fef945b.jpg', 'jogo'),
(4, 'Counter Strike: Global Offensive', 'Valve', 'PC / XBOX 360 / XBOX One / Playstation 3 /', 'FPS', '12', '21/08/2012', 'RzZ2bWZ_8Ho', '5ceb28afa40cffaa369b6c770e376668.jpg', 'jogo'),
(5, 'Overwatch', 'Blizzard Entertainment', 'PlayStation 4 / XBOX One / PC', 'FPS', '12', '24/05/2016', '1tnYpkt5G2g', 'fa135b1732b059b26afefab177270ee1.jpg', 'jogo'),
(6, 'FIFA 17', 'EA', 'PC /  PlayStation 3 / PlayStation 4 / XBOX 360 / XBOX One / IOS / Android ', 'Esporte / Simulação', '0', '27/09/2016', '-3fjoe5Njpc', 'a11d8a8f0a6ea9d963febd213ef61aa0.jpg', 'jogo'),
(7, 'Dota 2', 'Valve', 'PC', 'MOBA', '0', '09/07/2013', 'ti847WLgouQ', 'c06c8d948fd80d997d307f602f1f8510.jpg', 'jogo'),
(8, 'Fortnite', 'Epic Games', 'PC / PlayStation 4 / Xbox One', 'TPS / Construção / Sandbox / Battle Royal', '14', '25/07/2017 (Early Access)', 'NahGKWu0R7Y', '9c8e298e9b2ac80f902c37d2ba9e1f92.jpg', 'jogo'),
(9, 'PES 2018', 'KONAMI', 'PC /  PlayStation 3 / Playstation 4 / Xbox 360 / Xbox One', 'Esporte / Simulação', '0', '12/09/2017', 'jGgNxN0rQYw', '1ce06a3f263f99531f78f399354a2913.jpg', 'jogo'),
(10, 'Battlefield 4', 'EA', 'PC / Xbox 360 / PlayStation 3 PlayStation 4 / Xbox One', 'FPS', '18', '29/10/2013', 'sclTMEd7JN8', '7a9a6d357d8fd1305d11c89bdc27c812.jpg', 'jogo'),
(11, 'The Sims 4', 'EA', 'PC / XBOX One', 'Simulação', '14', '02/09/2014', 'z00mK3Pxc8w', '606e1dbc615d994a6e653326b8880a32.png', 'jogo'),
(12, 'Euro Truck Simulator 2', 'SCS Software', 'PC', 'Simulação', '0', '16/01/2013', 'xlTuC18xVII', '72929bfe97ba6dde81f6d22a581461fe.jpg', 'jogo'),
(13, 'Garrys Mod', 'Garry Newman', 'PC', 'RPG / Sandbox / Indie', '0', '24/12/2004', 'jDchc5Ww_i8', '98492596c5fe30df6ea111c4b360fbae.png', 'jogo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sugestoes_filmes`
--

DROP TABLE IF EXISTS `sugestoes_filmes`;
CREATE TABLE IF NOT EXISTS `sugestoes_filmes` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `lancamento` varchar(50) NOT NULL,
  `genero` varchar(150) NOT NULL,
  `elenco` varchar(500) NOT NULL,
  `direcao` varchar(50) NOT NULL,
  `video` varchar(200) NOT NULL,
  `cartaz` varchar(200) NOT NULL,
  `faixa` int(11) NOT NULL,
  `sinopse` text NOT NULL,
  `estado` varchar(20) NOT NULL DEFAULT 'pendente',
  `user` varchar(20) NOT NULL,
  `tipo` varchar(5) NOT NULL DEFAULT 'filme',
  PRIMARY KEY (`codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `sugestoes_filmes`
--

INSERT INTO `sugestoes_filmes` (`codigo`, `titulo`, `lancamento`, `genero`, `elenco`, `direcao`, `video`, `cartaz`, `faixa`, `sinopse`, `estado`, `user`, `tipo`) VALUES
(5, 'Liga da Justiça', '15/11/2017', 'Ação / Aventura / Fantasia / Ficção científica', 'Ben Affleck / Henry Cavill / Amy Adams / Gal Gadot / Ezra Miller / Jason Momoa / Ray Fisher / Jeremy Irons / Diane Lane / Connie Nielsen / J.K. Simmons', 'Zack Snyder', '9UCgwO0ft0Q', '83d7321cc2b7e78c25758898c9558bb3.jpg', 12, 'Impulsionado pela restauração de sua fé na humanidade e inspirado pelo ato altruísta do Superman (Henry Cavill), Bruce Wayne (Ben Affleck) convoca sua nova aliada Diana Prince (Gal Gadot) para o combate contra um inimigo ainda maior, recém-despertado. Juntos, Batman e Mulher-Maravilha buscam e recrutam com agilidade um time de meta-humanos, mas mesmo com a formação da liga de heróis sem precedentes - Batman, Mulher-Maraviha, Aquaman (Jason Momoa), Cyborg (Ray Fisher) e Flash (Ezra Miller) -, poderá ser tarde demais para salvar o planeta de um catastrófico ataque.', 'aceita', 'beric', 'filme'),
(6, 'O Lobo de Wall Street', '24/01/2014', 'Biografia / Drama / Policial', 'Leonardo Dicaprio', 'Martin Scorsese', 'PoSCUsNQVtw', 'b6c9bef8c48b1476b739425dafdc9df9.jpg', 18, 'Durante seis meses, Jordan Belfort (Leonardo DiCaprio) trabalhou duro em uma corretora de Wall Street, seguindo os ensinamentos de seu mentor Mark Hanna (Matthew McConaughey). Quando finalmente consegue ser contratado como corretor da firma, acontece o Black Monday, que faz com que as bolsas de vários países caiam repentinamente. Sem emprego e bastante ambicioso, ele acaba trabalhando para uma empresa de fundo de quintal que lida com papéis de baixo valor, que não estão na bolsa de valores. É lá que Belfort tem a ideia de montar uma empresa focada neste tipo de negócio, cujas vendas são de valores mais baixos mas, em compensação, o retorno para o corretor é bem mais vantajoso. Ao lado de Donnie (Jonah Hill) e outros amigos dos velhos tempos, ele cria a Stratton Oakmont, uma empresa que faz com que todos enriqueçam rapidamente e, também, levem uma vida dedicada ao prazer.', 'aceita', 'leo_sasse', 'filme');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sugestoes_jogos`
--

DROP TABLE IF EXISTS `sugestoes_jogos`;
CREATE TABLE IF NOT EXISTS `sugestoes_jogos` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(100) NOT NULL,
  `estudio` varchar(100) NOT NULL,
  `plataformas` varchar(150) NOT NULL,
  `genero` varchar(100) NOT NULL,
  `faixa` varchar(100) NOT NULL,
  `lancamento` varchar(100) NOT NULL,
  `video` varchar(200) NOT NULL,
  `cartaz` varchar(200) NOT NULL,
  `estado` varchar(20) NOT NULL DEFAULT 'pendente',
  `user` varchar(20) NOT NULL,
  `tipo` varchar(5) NOT NULL DEFAULT 'jogo',
  PRIMARY KEY (`codigo`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `sugestoes_jogos`
--

INSERT INTO `sugestoes_jogos` (`codigo`, `titulo`, `estudio`, `plataformas`, `genero`, `faixa`, `lancamento`, `video`, `cartaz`, `estado`, `user`, `tipo`) VALUES
(4, 'Cuphead', 'Studio MDHR', 'PC / XBOX ONE', 'Run and gun / Plataforma', '0', '29/09/2017', '4TjUPXAn2Rg', '5dc44d6ee17e61461e68a46d16c6fbf0.png', 'pendente', 'beric', 'jogo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `user` varchar(20) NOT NULL,
  `senha` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`user`, `senha`) VALUES
('admin', '21232f297a57a5a743894a0e4a801fc3'),
('eric', '4131f403beab0f4fa9e654b2ffa4f769'),
('gawhawhaw', '14ddf38c0e93fe1e45c863e0f2a7b128'),
('awnawnawn', 'c83b9688bd485d507a757d9d0733594d'),
('avawvah45qjejuq45j', 'f9a06c6a73aefe249e4ea5c1bc561e9b'),
('hrdhdrjaerdb', 'b76685f4bb0930ca966c62b2562ab2a0'),
('beric', '8378e7da76a8d91c8e00d3ba92a4089c'),
('aaa', 'd9f6e636e369552839e7bb8057aeb8da'),
('usuariocomum', '81dc9bdb52d04dc20036dbd8313ed055'),
('usercomum', '202cb962ac59075b964b07152d234b70'),
('fulano', '202cb962ac59075b964b07152d234b70'),
('joao', '62b8288c2867583bbbfc172290439710'),
('muhammad', '202cb962ac59075b964b07152d234b70'),
('ViníciusBraga', '9779c42d4e9088717048e15aea27f55b'),
('eric123', '39413293a6a7beb5faa6462dbb810b98'),
('testefav', '202cb962ac59075b964b07152d234b70'),
('leo_sasse', 'a993db879f6de2e22b9f3a267078ea7e');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
